/**
 * Dobygames MVC Framework
 *   Game has a Sequence Object.
 *   Sequence has 
 * 		an enchant.js core object,	
 *	and	Application Models,			(Model)
 * 	and enchant.js Scene Clases.	(View) 
 * 	and	Application Controllers.	(Controller)
 *
 *   Controller is assosiated EventTarget object.
 *   ex)	command1.addEventListener(enchant.Event.TOUCH_START, function(e) {
 * 			Game.Seq.StartingController.hoge();
 * 		}
 */

enchant();

var Game;					// Global Object

window.onload = function() {
	/**
	 *  ---------------------------------
	 *   enchant.js Application Settings
	 *  ---------------------------------
	 */
	var Seq = new Sequence();
	Seq.core.fps = 30;
	Seq.core.preload('images/icon0.png');
	Seq.core.preload('images/icon1.png');
	Seq.core.preload('images/icon2.png');
	Seq.core.preload('images/font2.png');
	Seq.core.preload('images/effect0.png');
	Seq.core.preload('images/scanner_logo.png');
	Seq.core.preload('images/dobygames64.png');
	Seq.core.preload('images/scanner/back1.png');
	Seq.core.preload('images/scanner/back2.png');
	Seq.core.preload('images/scanner/back_k.png');
	Seq.core.preload('images/scanner/back_ios.png');
	Seq.core.preload('images/scanner/back_ios2.png');
	Seq.core.preload('images/scanner/back_ios3.png');
	Seq.core.preload('images/scanner/back_ios4.png');
	Seq.core.preload('images/scanner/back_ios5.png');
	Seq.core.preload('images/scanner/back_ios9.png');
	Seq.core.preload('images/scanner/menu1.png');
	Seq.core.preload('images/scanner/menu2.png');
	Seq.core.preload('images/scanner/menu3.png');
	Seq.core.preload('images/scanner/menu4.png');
	Seq.core.preload('images/scanner/menu5.png');
	Seq.core.preload('images/scanner/menu9.png');
	Seq.core.preload('images/scanner/bugspot.png');
	Seq.core.preload('images/scanner/enemy.png');
	Seq.core.preload('images/scanner/player_k.png');
	Seq.core.preload('images/scanner/player_w.png');
	Seq.core.preload('images/scanner/enemyA.png');
	Seq.core.preload('images/scanner/gas1.png');
	Seq.core.preload('images/scanner/gas2.png');
	Seq.core.preload('images/scanner/gas3.png');
	Seq.core.preload('images/scanner/icon_star.png');
	Seq.core.preload('images/scanner/m_play.png');
	Seq.core.preload('images/scanner/m_stop.png');
	Seq.core.preload('images/scanner/m_line.png');
	Seq.core.preload('images/scanner/score.png');
	Seq.core.preload('images/scanner/next.png');
	Seq.core.preload('images/scanner/time.png');
	Seq.core.preload('images/scanner/cubes.png');
	Seq.core.preload('images/scanner/title.png');
	Seq.core.preload('images/scanner/start1.png');
	Seq.core.preload('images/scanner/start2.png');
	Seq.core.preload('images/scanner/tutorial1.png');
	Seq.core.preload('images/scanner/tutorial2.png');
	Seq.core.preload('images/scanner/nextbar.png');

	Seq.core.preload('images/scanner/clear.png');
	Seq.core.preload('images/scanner/drow.png');
	Seq.core.preload('images/scanner/gameover.png');
	Seq.core.preload('images/scanner/enemy1.png');
	Seq.core.preload('images/scanner/enemy2.png');
	Seq.core.preload('images/scanner/enemy3.png');
	Seq.core.preload('images/scanner/enemy4.png');
	Seq.core.preload('images/scanner/enemy5.png');
	Seq.core.preload('images/scanner/enemy6.png');
	Seq.core.preload('images/scanner/stage1.png');
	Seq.core.preload('images/scanner/stage2.png');
	Seq.core.preload('images/scanner/stage3.png');
	Seq.core.preload('images/scanner/stage4.png');
	Seq.core.preload('images/scanner/stage5.png');
	Seq.core.preload('images/scanner/stage6.png');

	Seq.core.preload('images/scanner/item.png');
	Seq.core.preload('images/scanner/go.png');
	Seq.core.preload('images/scanner/321.png');
	Seq.core.preload('images/scanner/funnel.png');
	Seq.core.preload('images/scanner/congrats.png');
	Seq.core.preload('images/scanner/arrow.png');

	/**
	 *  --------------------------------
	 *      Scanner Game Settings
	 *  --------------------------------
	 */
	Seq.core.onload = function() {
		// Game Settings
		Game.in_ready		= false;
		Game.in_option		= false;
		Game.in_score		= false;
		Game.in_tutorial	= false;
		Game.arcade			= true;
		Game.vs_mode		= true;
		Game.stage_fix		= true;
		Game.count_distance = false;
		Game.evaluator = new Evaluator();
		Game.evaluator.changeLevel(3);		// Normal Level
		Game.number_of_bug		= 0;
		Game.number_of_enemies  = 0;
		Game.stage_num  = 6;
		Game.red_rate   = 50;	// Max:100
		Game.enemy_bump_track  = 0;	
		Game.enemy_bump_bug	   = 0;	
		Game.blue_score	= 0;	
		Game.score_on_node	= 10;	
		Game.option	= false;	

		// Field Settings
		Game.FIELD_W  = 320;
		Game.FIELD_H  = 220;
		Game.SQUARE_W = 20;
		Game.SQUARE_H = 20;
		Game.FIELD_ROW  = 16;
		Game.FIELD_LINE = 11;
		Game.MENU_W  = 320;
		Game.MENU_H  = 100;

		// BackGround
		var field_back = new BackGround();
		field_back.changeImage(1);
		field_back.x = 0;
		field_back.y = 0;
		this.rootScene.addChild(field_back);
		Game.field_back = field_back;

		// MenuSkin
		var menu_back = new MenuSkin();
		menu_back.changeImage(1);
		menu_back.x = 0;
		menu_back.y = 220;
		this.rootScene.addChild(menu_back);
		Game.menu_back = menu_back;

		// Field
		var field = new Sprite(Game.FIELD_W, Game.FIELD_H);
		var surface	 = new Surface(Game.FIELD_W, Game.FIELD_H);
		surface.context.lineWidth	= 1;
		surface.context.lineJoin	= "round";
		field.image  = surface;	
		field.x = 10;
		field.y = 10;
		Game.surface = surface;

		// Track Field
		var track		= new Sprite(Game.FIELD_W+1, Game.FIELD_H+1);
		var surface2	= new Surface(Game.FIELD_W+1, Game.FIELD_H+1);
		track.image  = surface2;	
		track.x = 10;
		track.y = 10;
		track.visible = false;
		Game.surface2 = surface2;

		// Field 
		this.rootScene.addChild(track);
		this.rootScene.addChild(field);


		// Scanner 
		var SCANNER_W = 16;
		var SCANNER_H = 16;
		// Player
		var player = new Scanner(SCANNER_W, SCANNER_H, "player");
		this.rootScene.addChild(player);
		this.rootScene.player = player;
		Game.player = player;

		// NPC
		var npc = new Scanner(SCANNER_W, SCANNER_H, "npc");
		npc.ai_type = 3; // 1:random, 2:simpleAI, 3:routeSearchAI
		Game.npc = npc;

		// VS Mode
		Game.startVsMode = function() {
			Game.Seq.core.rootScene.addChild(npc);
		};
		Game.endVsMode = function() {
			Game.Seq.core.rootScene.removeChild(npc);
		};
		// 対戦モード開始
		if (Game.vs_mode) {
			Game.startVsMode();
		}


		// Flicker
		var flicker = new Flicker();
		this.rootScene.addChild(flicker);

		// Key Input (for PC)
		this.on('enterframe', function() {
			if (Game.Seq.core.input.right) {
				player.input = "right";
			} else if (Game.Seq.core.input.left) {
				player.input = "left";
			} else if (Game.Seq.core.input.up) {
				player.input = "up";
			} else if (Game.Seq.core.input.down) {
				player.input = "down";
			}
			// 逆方向へ移動しようとする行動を防ぐ
			if (player.dir=="right" && player.input=="left") player.input = player.orig_dir;
			if (player.dir=="left" && player.input=="right") player.input = player.orig_dir;
			if (player.dir=="up" && player.input=="down") player.input = player.orig_dir;
			if (player.dir=="down" && player.input=="up") player.input = player.orig_dir;
			// チュートリアル中に方向転換があれば、説明を閉じる
			//  if (Game.in_tutorial) {
			//  	console.log(player.input);
			//  	console.log(player.dir);
			//  	if (player.input != player.dir) {
			//  		Game.tutorial.close();
			//  	}
			//  }
			//  if (player.input != Game.Seq.core.input) {
			//  	// 入力報告
			//  	var input_icon = new Icon0();
			//  	input_icon.x = player.x;
			//  	input_icon.y = player.y;
			//  	input_icon.setDir(player.input);
			//  	input_icon.popUp();
			//  	Game.Seq.core.rootScene.addChild(input_icon);
			//  }
		});


		// Bug Spot
		var bugs = new Group();
		this.rootScene.addChild(bugs);
		this.rootScene.bugs = bugs;
		Game.bugs = bugs;
		Game.unsetBugSpot = function() {
			for (var i=Game.bugs.childNodes.length; i>=0; i--) {
				Game.bugs.removeChild(Game.bugs.childNodes[i]);
			}
		}
		Game.setBugSpot = function() {
			for (var j=0; j<Game.number_of_bug; j++) {
				var bug = new BugSpot(16, 16);
				bug.setRandomPos();
				Game.bugs.addChild(bug);
			}
		}

		// Enemy
		var enemies = new Group();
		this.rootScene.addChild(enemies);
		this.rootScene.enemies = enemies;
		Game.enemies = enemies;
		Game.unsetEnemy = function() {
			for (var i=Game.enemies.childNodes.length; i>=0; i--) {
				Game.enemies.removeChild(Game.enemies.childNodes[i]);
			}
		}
		Game.setEnemy = function() {
			for (var i=0; i<Game.number_of_enemies; i++) {
				var enemy = new Enemy(16, 16);
				enemy.setRandomPos();
				enemy.setRandomDir();
				Game.enemies.addChild(enemy);
			}
		}

		// Item
		var items = new Group();
		this.rootScene.addChild(items);
		Game.items = items;
		Game.unsetItem = function() {
			for (var i=Game.items.childNodes.length; i>=0; i--) {
				Game.items.removeChild(Game.items.childNodes[i]);
			}
		}


		// NextManager
		var next1 = new NextManager("player");
		next1.setQueue("player");
		next1.x = 30;
		next1.y = 215;
		this.rootScene.addChild(next1);
		this.rootScene.next1 = next1;
		Game.next1 = next1;
		var next2 = new NextManager("npc");
		next2.setQueue("npc");
		next2.x = 150 + 10;
		next2.y = 215;
		this.rootScene.addChild(next2);
		this.rootScene.next2 = next2;
		Game.next2 = next2;

		// Pause
		var pause = new Pause();
		pause.x = 235;
		pause.y = 268;
		this.rootScene.addChild(pause);
		Game.pause = pause;

		// Line
		var line_upper = new Sprite(277, 3);
		line_upper.image = Game.Seq.core.assets["images/scanner/m_line.png"];
		line_upper.x = 20;
		line_upper.y = 250;
		this.rootScene.addChild(line_upper);
		var line_downer = new Sprite(277, 3);
		line_downer.image = Game.Seq.core.assets["images/scanner/m_line.png"];
		line_downer.x = 20;
		line_downer.y = 310;
		this.rootScene.addChild(line_downer);

		// Text image
		var text1 = new Sprite(34, 11);
		text1.image = Game.Seq.core.assets["images/scanner/next.png"];
		text1.x = 160 - text1.width/2;
		text1.y = 237;
		this.rootScene.addChild(text1);
		var text2 = new Sprite(34, 11);
		text2.image = Game.Seq.core.assets["images/scanner/time.png"];
		text2.x = 35;
		text2.y = 287;
		this.rootScene.addChild(text2);
		var text3 = new Sprite(57, 12);
		text3.image = Game.Seq.core.assets["images/scanner/score.png"];
		text3.x = 35;
		text3.y = 267;
		this.rootScene.addChild(text3);

		// Score
		var score = new LabelCounter(50, 20);
		score.x = 100;
		score.y = 263;
		score.font = "16px Century";
		score.color = "white";
		score.setNumberOfDigit(6);
		score.setCountPerFrame(50);
		score.set(0);
		this.rootScene.addChild(score);
		this.rootScene.score = score;
		Game.score = score;

		// Timer
		var timer = new Timer(50, 20);
		timer.x = 110;
		timer.y = 283;
		this.rootScene.addChild(timer);
		Game.timer = timer;

		// Data
		//  var data = new LabelCounter(50, 20);
		//  data.x = 20
		//  data.y = 250
		//  data.setCountPerFrame(1);
		//  data.setBeforeText("Data ");
		//  data.setNumberOfDigit(5);
		//  data.set(0);
		//  this.rootScene.addChild(data);
		//  this.rootScene.data = data;

		// Distance
		//  var distance = new LabelCounter(50, 20);
		//  distance.x = 120
		//  distance.y = 250
		//  distance.setCountPerFrame(1);
		//  distance.setBeforeText("Distance ");
		//  distance.setNumberOfDigit(5);
		//  distance.set(0);
		//  this.rootScene.addChild(distance);
		//  this.rootScene.distance = distance;

		// Option Btn
		var opt_btn = new Sprite(64, 64);
		opt_btn.image = Game.Seq.core.assets["images/dobygames64.png"];
		opt_btn.x = 260; 
		opt_btn.y = 252;
		opt_btn.scaleX = 0.5;
		opt_btn.scaleY = 0.5;
		opt_btn.opacity = 0.5;
		opt_btn.addEventListener(enchant.Event.TOUCH_START, function(e) {
			if (Game.in_option == false) {
				this.opacity = 0.9;
				Game.in_option = true;
				option.show();
				option.y = -200;
				option.tl.moveTo(0,0,10, enchant.Easing.BACK_EASEIN);
			} else {
				this.opacity = 0.5;
				Game.in_option = false;
				option.tl.moveTo(0,-300,5, enchant.Easing.BACK_EASEIN);
				option.tl.fadeOut(3);
			}
		});
		if (Game.option) {
			this.rootScene.addChild(opt_btn);
		}


		// CountDown
		//  var countdown = new CountDown();
		//  this.rootScene.addChild(countdown);
		//  countdown.dispatch();


		// Option
		var OPTION_W = 320;
		var OPTION_H = 270;
		var option = new Group();
		option.x = 0;
		option.y = 0;
		option.hide = function() {
			for (var i=0; i<this.childNodes.length; i++) {
				var child = this.childNodes[i];
				child.visible = false;
			}
		};
		option.show = function() {
			for (var i=0; i<this.childNodes.length; i++) {
				var child = this.childNodes[i];
				child.visible = true;
			}
		};
		// Groupにwidgetを表示させる設定
		option._element = true;
		this.rootScene.addChild(option);
		// Window
		var wnd = new Sprite(OPTION_W, OPTION_H);
		wnd.surface = new Surface(OPTION_W, OPTION_H);
		wnd.surface.context.fillStyle = "rgba(0,0,0,0.9)";
		wnd.surface.context.fillRect(0, 0, OPTION_W, OPTION_H);
		var grad = wnd.surface.context.createLinearGradient(0, 0, OPTION_W, OPTION_H);
		grad.addColorStop(0, "rgb(255,255,0)");
		grad.addColorStop(0.25, "rgb(100,100,0)");
		grad.addColorStop(0.5, "rgb(255,255,0)");
		grad.addColorStop(0.75, "rgb(100,100,0)");
		grad.addColorStop(1, "rgb(255,255,0)");
		wnd.surface.context.strokeStyle = grad;
		wnd.surface.context.lineWidth = 5;
		wnd.surface.context.lineJoin  = "round";
		wnd.surface.context.strokeRect(0, 0, OPTION_W, OPTION_H);
		wnd.image = wnd.surface;
		option.addChild(wnd);
		// Commands
		var label = new Label(50, 20);
		label.text = "Game Developing Option";
		label.font = "16px KozMinPro-Regular";
		label.color = "white";
		label.x = 10;
		label.y = 10;
		option.addChild(label);
		// Track Color Interval
		var label1 = new Label(50, 20);
		label1.text = "Track Color Interval";
		label1.font = "14px KozMinPro-Regular";
		label1.color = "white";
		label1.x = 20;
		label1.y = 1 * 40 + 10;
		option.addChild(label1);
		var cmd1_opt = {0:0,1:1,2:2,3:3,4:4,5:5,6:6};
		var command1 = new InputSelectBox(cmd1_opt);
		command1.x = 200;
		command1.y = 1 * 40 + 6;
		command1.selected = player.interval;
		command1.addEventListener(enchant.Event.CHANGE, function(){
			player.interval = command1.selected;
		  	if (Game.vs_mode) {
				npc.interval = command1.selected;
			}
			// 赤のみの場合の処理（オリジナルのタッチ＆ゴー）
			if (command1.selected == 0) {
				next1.hide();
				next2.hide();
			} else {
				next1.show();
				next2.show();
			}
		});
		option.addChild(command1);
		// Speed
		var label2 = new Label(50, 20);
		label2.text = "Speed";
		label2.font = "14px KozMinPro-Regular";
		label2.color = "white";
		label2.x = 20;
		label2.y = 2 * 40 + 10;
		option.addChild(label2);
		var cmd2_opt = {1:"normal",2:"fast"}; // TODO 速さの微調節
		var command2 = new InputSelectBox(cmd2_opt);
		command2.x = 200;
		command2.y = 2 * 40 + 6;
		command2.selected = player.speed;
		command2.addEventListener(enchant.Event.CHANGE, function(){
			player.speed = parseInt(command2.selected);
			if (player.speed == 2) {
				if ((player.x+3)%2==1)	player.x+=1; // TODO 線の交点の判定を見直す
				if ((player.y+3)%2==1)	player.y+=1;
			}
			// console.log(command2.selected);
			// console.log(player.speed);
		});
		option.addChild(command2);
		// BugSpot
		var label3 = new Label(50, 20);
		label3.text = "BugSpot";
		label3.font = "14px KozMinPro-Regular";
		label3.color = "white";
		label3.x = 20;
		label3.y = 3 * 40 + 10;
		option.addChild(label3);
		var cmd3_opt = {0:0,5:5,10:10,15:15,20:20,25:25,30:30};
		var command3 = new InputSelectBox(cmd3_opt);
		command3.x = 200;
		command3.y = 3 * 40 + 6;
		command3.selected = Game.number_of_bug;
		command3.addEventListener(enchant.Event.CHANGE, function(){
			Game.number_of_bug = command3.selected;
			Game.unsetBugSpot();
			Game.setBugSpot();
		});
		option.addChild(command3);
		// Count Distance
		//  var label4 = new Label(50, 20);
		//  label4.text = "Count Distance";
		//  label4.font = "14px KozMinPro-Regular";
		//  label4.color = "white";
		//  label4.x = 20;
		//  label4.y = 4 * 40 + 10;
		//  option.addChild(label4);
		//  var cmd4_opt = {false:"false",true:"true"};
		//  var command4 = new InputSelectBox(cmd4_opt);
		//  command4.x = 200;
		//  command4.y = 4 * 40 + 6;
		//  command4.selected = Game.count_distance;
		//  command4.addEventListener(enchant.Event.CHANGE, function(){
		//  	Game.count_distance = command4.selected;
		//  });
		//  option.addChild(command4);

		// Stage
		var label4 = new Label(50, 20);
		label4.text = "Stage";
		label4.font = "14px KozMinPro-Regular";
		label4.color = "white";
		label4.x = 20;
		label4.y = 4 * 40 + 10;
		option.addChild(label4);
		var cmd4_opt = {1:1, 2:2, 3:3, 4:4, 5:5, 6:6};
		var command4 = new InputSelectBox(cmd4_opt);
		command4.x = 200;
		command4.y = 4 * 40 + 6;
		command4.selected = 0;
		command4.addEventListener(enchant.Event.CHANGE, function(){
			var num = Number(command4.selected);
			// ステージデータ
			Game.manager.stage_id = num -1;
			Game.manager.next();
		});
		option.addChild(command4);

		// Enemy
		var label5 = new Label(50, 20);
		label5.text = "Enemy";
		label5.font = "14px KozMinPro-Regular";
		label5.color = "white";
		label5.x = 20;
		label5.y = 5 * 40 + 10;
		option.addChild(label5);
		var cmd5_opt = {0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9};
		var command5 = new InputSelectBox(cmd5_opt);
		command5.x = 200;
		command5.y = 5 * 40 + 6;
		command5.selected = Game.number_of_enemies;
		command5.addEventListener(enchant.Event.CHANGE, function(){
			Game.number_of_enemies = command5.selected;
			Game.unsetEnemy();
			Game.setEnemy();
		});
		option.addChild(command5);

		// VS Mode
		//  var label6 = new Label(50, 20);
		//  label6.text = "VS Mode";
		//  label6.font = "14px KozMinPro-Regular";
		//  label6.color = "white";
		//  label6.x = 20;
		//  label6.y = 6 * 40 + 10;
		//  option.addChild(label6);
		//  var cmd6_opt = {false:"false",true:"true"};
		//  var command6 = new InputSelectBox(cmd6_opt);
		//  command6.x = 200;
		//  command6.y = 6 * 40 + 6;
		//  command6.selected = "true";
		//  command6.addEventListener(enchant.Event.CHANGE, function(){
		//  	if (Game.vs_mode) {
		//  		Game.vs_mode = false;
		//  		Game.endVsMode();
		//  	} else {
		//  		Game.vs_mode = true;
		//  		Game.startVsMode();
		//  	}
		//  });
		//  option.addChild(command6);

		// NPC Level (対戦モードはデフォルトにする)
		var label6 = new Label(50, 20);
		label6.text = "NPC Level";
		label6.font = "14px KozMinPro-Regular";
		label6.color = "white";
		label6.x = 20;
		label6.y = 6 * 40 + 10;
		option.addChild(label6);
		var cmd6_opt = {
			1:Game.evaluator.label[1],
			2:Game.evaluator.label[2],
			3:Game.evaluator.label[3],
			4:Game.evaluator.label[4],
			5:Game.evaluator.label[5],
		};
		var command6 = new InputSelectBox(cmd6_opt);
		command6.x = 200;
		command6.y = 6 * 40 + 6;
		command6.selected = 3; // Normal
		command6.addEventListener(enchant.Event.CHANGE, function(){
			var value = command6.selected;
			Game.evaluator.changeLevel(value);
			console.log(Game.evaluator);
		});
		option.addChild(command6);

		// Optionを隠す
		option.hide();


/**
 * ClearBoard
 */
var ClearBoard = enchant.Class.create(enchant.Group, {
	initialize: function() {
		Group.call(this);
		// Property

		// Window
		var WINDOW_W = 320;
		var WINDOW_H = 320;
		this.wnd = new Sprite(WINDOW_W, WINDOW_H);
		this.wnd.surface = new Surface(WINDOW_W, WINDOW_H);
		this.wnd.surface.context.fillStyle = "rgba(0,0,0,0.6)";
		this.wnd.surface.context.fillRect(0, 0, WINDOW_W, WINDOW_H);
		this.wnd.image = this.wnd.surface;
		this.addChild(this.wnd);

		// Label 
		this.labels = new Group();
		this.addChild(this.labels);

		// StageClear
		this.addLabel(0, 20, "STAGE CLEAR", {"font":"20px Century", "textAlign":"center"});

		// Score
		this.addLabel(40, 80, "SCORE");
		this.addLabel(200, 80, Game.score.value);
		// Time Bonus
		this.addLabel(60, 110, "TIME");
		var spent	= Game.timer.getTime();
		var rank	= this.getRank(spent);
		var time_bonus = this.getTimeBonus(rank);
		var time_txt   = this.getTimeText(rank);
		this.addLabel(200, 110, time_bonus + time_txt);
		// Clear Bonus
		this.addLabel(60, 140, "CLEAR");
		var clear_bonus = this.getClearBonus();
		var clear_txt   = this.getClearText();
		this.addLabel(200, 140, clear_bonus + clear_txt);
		// Stage Score
		this.addLabel(40, 180, "STAGE" + Game.manager.stage_id);
		var stage_score = Game.score.value + time_bonus + clear_bonus;
		this.addLabel(200, 180, stage_score);
		// Total Score
		this.addLabel(40, 220, "TOTAL");
		Game.manager.total_score += stage_score;
		this.addLabel(200, 220, Game.manager.total_score);

		// Continue
		this.addLabel(0, 260, "CONTINUE", {"font":"16px Century", "textAlign":"center"});
		this.continue_btn = this.getLabel();
		this.continue_btn.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.manager.next();
		});

		// スコアを非表示にする
		for (var i=0; i<this.labels.childNodes.length; i++) {
			if (i==0) continue;
			this.labels.childNodes[i].visible = false;
		}
		// Animation
		this.tl.cue({
			15:function(){
				this.showLabel([1,2]);
			},
			30:function(){
				this.showLabel([3,4]);
			},
			45:function(){
				this.showLabel([5,6]);
			},
			60:function(){
				this.showLabel([7,8]);
			},
			75:function(){
				this.showLabel([9,10]);
			},
			105:function(){
				this.showLabel([11]);
			},
		});
	},
	showLabel: function(args) {
		if (typeof args === "object") {
			for (var i=0; i<args.length; i++) {
				var index = args[i];
				this.labels.childNodes[index].visible = true;
			}
		} else if (typeof args === "number") {
			var index = args;
			this.labels.childNodes[index].visible = true;
		}
	},
	getRank: function(time) {
		if (time <= 20)			return "A";
		else if (time <= 30)	return "B";
		else if (time <= 40)	return "C";
	},
	getRankColor: function(rank) {
		if (rank=="A")			return "red";
		else if (rank=="B")		return "yellow";
		else if (rank=="C")		return "green";
	},
	getTimeBonus: function(rank) {
		if (rank=="A")			return 4000;
		else if (rank=="B")		return 2000;
		else if (rank=="C")		return 1000;
	},
	getTimeText: function(rank) {
		if (rank=="A")			return "(20 under)";
		else if (rank=="B")		return "(40 under)";
		else if (rank=="C")		return "(60 under)";
	},
	getClearBonus: function() {
		if (Game.npc.track_dead)		return 2000;
		else if (Game.npc.bug_dead)		return 1000;
		else if (Game.npc.enemy_dead)	return 500;
		else return 0;
	},
	getClearText: function() {
		if (Game.npc.track_dead)		return "(Track)";
		else if (Game.npc.bug_dead)		return "(BugSpot)";
		else if (Game.npc.enemy_dead)	return "(Enemy)";
		else return "";
	},
	addLabel: function(x, y, value, option) {
		var label = new Label(50, 20);
		label.x = x;
		label.y = y;
		label.text  = value;
		label.font  = "16px Century";
		label.color = "white";
		// 色、フォント、水平位置を指定できる
		for (key in option) {
			if (key=="color")		label.color = option[key];
			if (key=="font")		label.font  = option[key];
			if (key=="textAlign")	label.textAlign = option[key];
		}
		this.labels.addChild(label);
	},
	getLabel: function(index) {
		if (typeof index === 'undefined') {
			index = this.labels.childNodes.length - 1;
		}
		return this.labels.childNodes[index];
	}
});


/**
 * StageData
 */
var StageData = enchant.Class.create(enchant, {
	initialize: function() {
		// Property
		this.stage_arr = [
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		];
	},
	getData: function(stage_id) {
		switch (stage_id) {
			case 0:	
				this.stage_arr = [
[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
[0,0,0,0,0,0,1,0,0,0,1,0,0,1,0,0],
[0,0,1,0,0,0,0,0,0,0,0,0,1,1,1,0],
[0,0,0,1,0,1,0,0,1,0,0,0,0,1,0,0],
[0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0],
[0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,1,0,1,0,0,1,0,0,0,0,0,0,1,0,0],
[0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0],
[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]
				];
				break;
			case 1:	
				this.stage_arr = [
//１面 固定　多い　Lv3 　遅い　進路選択型　「勝って欲しいので左端戦法が使える」
[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
[0,0,1,0,0,0,1,0,0,0,0,0,1,1,0,0],
[0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0],
[0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0],
[0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0],
[0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
[0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0],
[0,0,1,1,0,0,0,0,0,1,0,0,0,1,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0]
				];
				break;
			case 2:	
				this.stage_arr = [
//２面　ランダム　少ない(5)　Lv4　遅い　自由行動型　青多い　「青トラックを使う楽しさ」
[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
[0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
[0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
[0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]
				];
				break;
			case 3:	
				this.stage_arr = [
//３面　動く　多い　Lv3　普通　逃避型　「避ゲーとして」
[0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0],
[0,0,0,0,0,0,0,2,0,0,2,0,0,0,0,0],
[0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,1,0,0,0,0,2,0,0,0,0,0],
[0,0,0,2,0,0,0,0,0,2,0,0,0,0,2,0],
[0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0],
[0,0,0,2,0,0,2,0,0,1,0,0,2,0,0,0],
[0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0],
[0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0],
[0,2,0,0,0,2,0,2,0,0,0,0,2,0,0,0],
[0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0]
				];
				break;
			case 4:	
				this.stage_arr = [
//４面　固定　少ない　Lv5　早い　自由行動型　青多い　「青トラックを敵もしっかり使ってくる」
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
				];
				break;
			case 5:	
				this.stage_arr = [
//５面　ランダム　多い（３０） Lv5　早い　自由行動型　「ステージを読み解いて倒す楽しさ」
[0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
[0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0],
[0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0],
[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
[0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0],
[0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0],
[0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0],
[0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0],
[0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
[0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0]
				];
				break;
			case 6:	
				this.stage_arr = [
//６面　固定＆動く　多い　Lv3　早い　進路選択型？　敵青多い　「今までの全要素を入れる」
[0,1,0,0,0,0,0,0,2,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0],
[0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0],
[0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0],
[0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0],
[0,1,0,0,0,0,2,0,0,1,0,1,0,0,0,0],
[0,0,0,0,2,0,0,0,0,1,0,1,0,0,0,0],
[0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0]
				];
				break;
		}
		return this.stage_arr;

	},
	setSpeed: function(stage_id) {
		switch (stage_id) {
			case 0:
				Game.player.speed_level = 1;
				Game.npc.speed_level    = 1;
				break;
			case 1:
				Game.player.speed_level = 2;
				Game.npc.speed_level    = 2;
				break;
			case 2:
				Game.player.speed_level = 3;
				Game.npc.speed_level    = 3;
				break;
			case 3:
				Game.player.speed_level = 5;
				Game.npc.speed_level    = 1;
				break;
			case 4:
				Game.player.speed_level = 1;
				Game.npc.speed_level    = 3;
				break;
			case 5:
				Game.player.speed_level = 3;
				Game.npc.speed_level    = 3;
				break;
			case 6:
				Game.player.speed_level = 1;
				Game.npc.speed_level    = 3;
				break;
		}
		Game.player.getSpeed();
		Game.npc.getSpeed();
	},
	setLevel: function(stage_id) {
		switch (this.stage_id) {
			case 0:
				Game.evaluator.changeLevel(3);
				break;
			case 1:
				Game.evaluator.changeLevel(1);
				break;
			case 2:
				Game.evaluator.changeLevel(4);
				break;
			case 3:
				Game.evaluator.changeLevel(3);
				break;
			case 4:
				Game.evaluator.changeLevel(5);
				break;
			case 5:
				Game.evaluator.changeLevel(5);
				break;
			case 6:
				Game.evaluator.changeLevel(3);
				break;
		}
	}

});



/**
 * Tutorial
 */
var Tutorial = enchant.Class.create(enchant.Group, {
	initialize: function() {
		Group.call(this);
		// Property
		this.text = "";
		this.first = true;
		this.maru = null;
		this.enemy_tutorial = false;

		// Window
		var WINDOW_W = 320;
		var WINDOW_H = 100;
		this.wnd = new Sprite(WINDOW_W, WINDOW_H);
		this.wnd.surface = new Surface(WINDOW_W, WINDOW_H);
		this.wnd.surface.context.fillStyle = "rgba(0,0,0,0.8)";
		this.wnd.surface.context.fillRect(0, 0, WINDOW_W, WINDOW_H);
		this.wnd.image = this.wnd.surface;
		this.addChild(this.wnd);
		this.wnd.x = 0;
		this.wnd.y = 220;
		// タッチすると再開する
		this.wnd.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.tutorial.close();
		});
		// Text
		this.label = new Label(50, 20);
		this.label.x = 10;
		this.label.y = 260;
		this.label.text  = "こんにちは";
		//this.label.font  = "Bold 16px Century";
		this.label.font = "bold 13px 'Helvetica','Arial','sans-serif'";
		this.label.color = "white";
		this.label.textAlign = "center";
		this.label.backgroundColor = "rgb(0,0,0)";
		this.label.touchEnabled = false;
		this.addChild(this.label);
		// 初期状態は非表示にする
		this.hide();

		// 自機と敵機の座標と色を固定する
		Game.player.x = 100 + 3;
		Game.player.y = 80 + 3;
		Game.player.color = "red";
		Game.player.queue = ["red", "blue"];
		Game.next1.setQueue("player");
		Game.npc.x = 200 + 3;
		Game.npc.y = 120 + 3;
		Game.npc.color = "blue";
		Game.npc.queue = ["red", "red"];
		Game.next2.setQueue("npc");

		this.tl.cue({
			1:function(){
				this.setText("チュートリアルへようこそ！<br>画面をタッチしてください。");
			},
			30:function(){
				this.setText("バグスポットに当たると、<br>GAMEOVERになってしまいます。");
				this.focus(216, 76);
			},
			32:function(){
				this.setText("赤トラックも当たると、GAMEOVERです。<br>");
				this.focus(110, 76);
			},
			60:function(){
				this.setText("画面上でフリックすると、<br>自機が左右に方向転換します。<br>（PCの場合は方向キー）");
				this.focus(Game.player.x-6, Game.player.y-6);
			},
			70:function(){
				this.setText("青トラックを踏むと、<br>エネミーが出現し敵機を追跡します。<br>下にフリックしてみましょう。");
				this.focus(175, 116);
			},
			240:function(){
				this.setText("赤トラックで相手を囲む、<br>青トラックを踏んで逃げ道をうばう・・");
			},
			245:function(){
				this.setText("トラックをうまく使って、<br>敵を追いつめてください");
			},
		});
	},
	setText: function(str) {
		// ゲームを一時停止させる
		Game.pause.pause();
		// 黒背景とテキストを表示
		this.show();
		// テキストを更新する
		this.label.text = str;
	},
	close: function() {
		this.hide();
		Game.pause.resume();	
		if (this.maru) {
			this.maru.remove();
		}
	},
	hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			this.childNodes[i].visible = false;
		}
	},
	show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			this.childNodes[i].visible = true;
		}
	},
	focus: function(x,y) {
		var maru = new Sprite(30,30);
		var surface = new Surface(30,30);
		surface.context.beginPath();
		surface.context.arc(15,15,14,0,Math.PI * 2);
		surface.context.lineWidth = 2;
		surface.context.strokeStyle="red";
		surface.context.stroke();
		maru.image = surface;
		maru.x = x;
		maru.y = y;
		Game.Seq.core.rootScene.addChild(maru);
		this.maru = maru;
	}
});


/**
 * GameManager
 */
var GameManager = enchant.Class.create({
	initialize: function() {
		// Property
		this.stage_id	  = 0;
		this.total_score  = 0;
		this.stage_data   = new StageData();
		this.clear_flag   = false;
	},
	setTitle: function() {
		Game.title_scene = new TitleScene();
		Game.Seq.core.pushScene(Game.title_scene);
	},
	initStage: function() {
		// プレイヤーの初期化
		Game.player.setProperty("player");
		Game.player.setPos("player");
		Game.player.setType("player");
		if (Game.vs_mode) {
			Game.npc.setProperty("npc");
			Game.npc.setPos("npc");
			Game.npc.setType("npc");
		}
		// トラックの初期化
		Game.surface.clear();
		Game.surface2.clear();
		// 経過カウントの初期化
		Game.player.count = 0;
		Game.npc.count	  = 0;
		// キューの初期化
		Game.player.setColor();
		Game.npc.setColor();
		Game.next1.setQueue("player");
		Game.next2.setQueue("npc");
		Game.next1.balls[0].tl.scaleTo(1,1,1);
		Game.next2.balls[0].tl.scaleTo(1,1,1);
		// 障害物の初期化
		Game.unsetEnemy();
		Game.unsetBugSpot();
		Game.unsetItem();
		// スコアの初期化
		Game.score.set(0);
		// タイマーの初期化
		Game.timer.setStartTime();
		Game.timer.initTime();
	},
	setStage: function() {
		// 固定ステージの場合
		if (Game.stage_fix) {
			this.setObstacle();
		} else {
		// ランダムの場合
			Game.setEnemy();
			Game.setBugSpot();
		}
	},
	setCountDown: function() {
		Game.countdown = new CountDown();
		Game.Seq.core.rootScene.addChild(Game.countdown);
		Game.countdown.dispatch();
	},
	setTutorial: function() {
		Game.tutorial = new Tutorial();
		Game.Seq.core.rootScene.addChild(Game.tutorial);
		Game.in_tutorial = true;
		// チュートリアルはstage_id=0で障害物をおく
		this.setObstacle();
	},
	setObstacle: function() {
		var id = this.stage_id;
		var stage_data = this.stage_data.getData(id);
		// ステージデータからバグスポットとエネミーを出現させる
		for (var i=0; i<Game.FIELD_LINE; i++) {
			for (var j=0; j<Game.FIELD_ROW; j++) {
				var state = stage_data[i][j];
				if (state==1) {
					var bug = new BugSpot(16, 16);
					bug.x = j * 20 + 3;
					bug.y = i * 20 + 3;
					Game.bugs.addChild(bug);
				} else if (state==2) {
					var enemy = new Enemy(16, 16);
					enemy.x = j * 20 + 3;
					enemy.y = i * 20 + 3;
					Game.enemies.addChild(enemy);
				}
			}
		}
	},
	gameClear: function() {
		var score = Number(Game.manager.total_score);
		Game.Seq.core.end(score, 'あなたのスコアは' + score + 'です');
	},
	clear: function() {
		// スコア中
		Game.in_score = true;
		// WIN表示
		var img_win = Game.Seq.core.assets["images/scanner/clear.png"];
		var win = new Sprite(img_win.width, img_win.height);
		win.image = img_win;
		win.x = (320 - win.width) / 2;
		win.y = (320 - win.height) / 2;
		win.opacity = 0;
		win.tl.fadeIn(20);
		Game.Seq.core.rootScene.addChild(win);
		// WINをタッチしたときの処理
		win.addEventListener(enchant.Event.TOUCH_START, function(e) {
			this.parentNode.removeChild(this);
			// チュートリアルの場合、トップにもどる
			if (Game.in_tutorial) {
				var score = Number(Game.manager.total_score + Game.score.value);
				Game.Seq.core.end(score, 'チュートリアルをクリアしました！あなたのスコアは' + score + 'です');
				return;
			} else {
			// 通常の場合、クリアボードを表示する
				Game.clearboard = new ClearBoard();
				Game.Seq.core.rootScene.addChild(Game.clearboard);
			}
		});
	},
	fail: function() {
		// GAMEOVER表示
		var img_lose = Game.Seq.core.assets["images/scanner/gameover.png"];
		var lose = new Sprite(img_lose.width, img_lose.height);
		lose.image = img_lose;
		lose.x = (320 - lose.width) / 2;
		lose.y = (320 - lose.height) / 2;
		Game.Seq.core.rootScene.addChild(lose);
		var score = Number(Game.manager.total_score + Game.score.value);
		Game.Seq.core.end(score, 'あなたのスコアは' + score + 'です');
	},
	next: function() {
		// クリアボードを消す
		if (Game.in_score) {
			Game.in_score = false;
			Game.Seq.core.rootScene.removeChild(Game.clearboard);
		}
		// ステージの初期化
		this.initStage();
		// ステージ番号の繰り上げ
		this.stage_id += 1;
		// ゲームクリアの判定
		if (this.stage_id == Game.stage_num+1) this.clear_flag = true;
		// ゲームクリアの処理 
		if (this.clear_flag) {
			// Congra!を表示する
			Game.cong_scene = new CongScene();
			Game.Seq.core.pushScene(Game.cong_scene);
			return;	
		}

		// NPCのレベルを変更する
		this.changeLevel();
		// ステージの背景を変更する
		this.changeStage();
		// 障害物を設置する
		this.setStage();
		// ステージ番号の表示
		this.stage_label = new Sprite(320, 92);
		this.stage_label.image = Game.Seq.core.assets["images/scanner/stage" + this.stage_id + ".png"];
		this.stage_label.x = -10;
		this.stage_label.y = 80;
		this.stage_label.opacity = 0;
		Game.Seq.core.rootScene.addChild(this.stage_label);

		var target_x = (320 - this.stage_label.width) / 2;
		this.stage_label.tl.moveTo(target_x,80,30)
						   .and().fadeIn(30)
						   .delay(10)
						   .fadeOut(10);
		// カウントダウン中にする
		Game.in_ready = true;
		// アニメーション
		this.stage_label.tl.cue({
			10:function(){
				// カウントダウン開始
				Game.manager.setCountDown();
				// ステージ番号表示
				this.parentNode.removeChild(this);
			},
		});
	},
	setMode: function(mode) {
		if (mode=="test")			Game.arcade = false;
		else if (mode=="arcade")	Game.arcade = true;
	},
	changeStage: function() {
		var stage_id = Number(this.stage_id);
		Game.field_back.changeImage(stage_id);
		Game.menu_back.changeImage(stage_id);
	},
	changeLevel: function() {
		// NPCの画像を変更する
		Game.npc.setEnemyImage(this.stage_id);
		// NPCのレベルを変更する
		this.stage_data.setLevel(this.stage_id);
		// スピードを変更する
		this.stage_data.setSpeed(this.stage_id);
	},
	execStart: function() {
		this.next();
	},
	execTutorial: function() {
		this.setTutorial();
	}
});
		// GameManager
		Game.manager = new GameManager();
		// Game.manager.initStage();
		// Game.manager.setCountDown();
		// Game.manager.clear();
		// Game.manager.next();
		// Game.manager.setTutorial();
		// Game.manager.setTitle();

		Game.title_scene = new TitleScene();
		this.pushScene(Game.title_scene);

		// Game.cong_scene = new CongScene();
		// this.pushScene(Game.cong_scene);

	}


	/**
	 *  enchant.js Game Start
	 */
	Seq.core.start();
	Game.Seq = Seq;
};

