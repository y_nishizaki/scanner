/**
 * Effect.js
 *  - Icon0.js
 *  - Icon2.js
 *  - Item.js
 *  - Bomb.js
 */


/**
 * Icon0.js
 */
var Icon0 = enchant.Class.create(enchant.Sprite, {
	initialize: function(type) {
		Sprite.call(this, 16, 16);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/arrow.png"];
		this.frame = 0; // SPD:606; // AGI:46;
	},
	setDir: function(dir) {
		//  if (dir=="right")	   this.rotation = 90;
		//  else if (dir=="left")  this.rotation = 270;
		//  else if (dir=="up")    this.rotation = 0;
		//  else if (dir=="down")  this.rotation = 180;
		if (dir=="right")	   this.frame = 1;
		else if (dir=="left")  this.frame = 2;
		else if (dir=="up")    this.frame = 0;
		else if (dir=="down")  this.frame = 3;
	},
	popUp: function() {
		this.tl.moveBy(0,-10,15,enchant.Easing.CIRC_EASEOUT)
			   .fadeOut(5);
	}
});


/**
 * Icon2.js
 */
var Icon2 = enchant.Class.create(enchant.Sprite, {
	initialize: function(type) {
		Sprite.call(this, 24, 24);
		// Property
		this.image = Game.Seq.core.assets["images/icon2.png"];
		this.frame = 46; // SPD:606; // AGI:46;
	},
	speedUp: function() {
		this.frame = 46; // SPD:606; // AGI:46;
	},
	summonEnemy: function() {
		this.frame = 669; // wolf:669; // stecki:152; 
	},
	showHand: function() {
		this.frame = 482; // hand:482; 
	},
	showEnemy: function() {
		this.frame = 17; // punch:27; // dragon:25; 
	},
	popUp: function() {
		this.tl.moveBy(0,-10,15,enchant.Easing.CIRC_EASEOUT)
			   .fadeOut(5);
	},
	moveHand: function() {
		this.tl.moveBy(20,0,25,enchant.Easing.CIRC_EASEOUT)
			   .moveBy(-20,0,1,enchant.Easing.CIRC_EASEOUT)
			   .moveBy(0,20,25,enchant.Easing.CIRC_EASEOUT)
			   .fadeOut(5);
	}
});


/**
 * Gas.js
 */
var Gas = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		Sprite.call(this, 27, 27);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/gas3.png"];
		this.frame = 0; 
		this.scaleX = 0.1;
		this.scaleY = 0.1;
		this.opacity = 0.3;
		this.originY = 13.5;
		this.originY = 13.5;
		this.buttonMode = false;
	},
	wave: function(color) {
		if (color=="red") this.frame=1;
		else if (color=="blue") this.frame=2;
		else if (color=="yellow") this.frame=0;
		this.tl.scaleTo(3,3,6,enchant.Easing.CIRC_EASEOUT)
			   .and().fadeOut(6)
			   .removeFromScene();
	},
});


/**
 * Item.js
 */
var Item = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		Sprite.call(this, 16, 16);
		// Image
		this.image = Game.Seq.core.assets["images/scanner/item.png"];
		this.frame = 1;
		// Property
		this.get = false;
		this.radius = 1;
		// 演出
		this.opacity = 0;
		this.tl.fadeIn(20);
	},
	onenterframe: function() {
		if (this.age<5)	return false;
		// 衝突判定
		var bump = false;
		if (this.within(Game.player, 1)) bump = true;
		if (Game.vs_mode) {
			if (this.within(Game.npc, 1))	bump = true;
		}
		// 消滅
		if (bump && this.get==false) {
			this.get = true;
			this.tl.fadeOut(20)
				   .removeFromScene();
		}
	}
});


/**
 * Bomb.js
 */
var Bomb = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		width = height = 16;
		Sprite.call(this, width, height);
		// Imape
		this.image = Game.Seq.core.assets["images/effect0.png"];
		this.frame = 0;
	}, 
	onenterframe: function() {
		this.frame += 1;
		if (this.frame >= 5) {
			this.parentNode.removeChild(this);
		}
	}
});
