/**
 * Title.js
 *  - Cube
 *  - TitleScene
 */

/**
 * Cube
 */
var Cube = enchant.Class.create(enchant.Sprite, {
	initialize: function(index, x, y) {
		enchant.Sprite.call(this, 16, 16);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/cubes.png"];
		this.frame = index - 1;
		this.x = x;
		this.y = y;
	}
});


/**
 * TitleScene
 */
var TitleScene = enchant.Class.create(enchant.Scene, {
	initialize: function() {
		enchant.Scene.call(this);
		this.selected = false;

		this.title = new Sprite();
		this.title.width  = Game.Seq.core.assets['images/scanner/title.png'].width;
		this.title.height = Game.Seq.core.assets['images/scanner/title.png'].height;
		this.title.image  = Game.Seq.core.assets['images/scanner/title.png'];
		this.title.x = 0;
		this.title.y = 0;
		this.addChild(this.title);

		this.start1 = new Sprite();
		this.start1.width  = Game.Seq.core.assets['images/scanner/start1.png'].width;
		this.start1.height = Game.Seq.core.assets['images/scanner/start1.png'].height;
		this.start1.image  = Game.Seq.core.assets['images/scanner/start1.png'];
		this.start1.x = 161 - this.start1.width / 2;
		this.start1.y = 226;
		this.addChild(this.start1);

		this.start2 = new Sprite();
		this.start2.width  = Game.Seq.core.assets['images/scanner/start2.png'].width;
		this.start2.height = Game.Seq.core.assets['images/scanner/start2.png'].height;
		this.start2.image  = Game.Seq.core.assets['images/scanner/start2.png'];
		this.start2.x = 161 - this.start2.width / 2;
		this.start2.y = 226;
		this.start2.touchEnabled = false;
		this.addChild(this.start2);

		this.tutorial1 = new Sprite();
		this.tutorial1.width  = Game.Seq.core.assets['images/scanner/tutorial1.png'].width;
		this.tutorial1.height = Game.Seq.core.assets['images/scanner/tutorial1.png'].height;
		this.tutorial1.image  = Game.Seq.core.assets['images/scanner/tutorial1.png'];
		this.tutorial1.x = 159 - this.tutorial1.width / 2;
		this.tutorial1.y = 278;
		this.addChild(this.tutorial1);

		this.tutorial2 = new Sprite();
		this.tutorial2.width  = Game.Seq.core.assets['images/scanner/tutorial2.png'].width;
		this.tutorial2.height = Game.Seq.core.assets['images/scanner/tutorial2.png'].height;
		this.tutorial2.image  = Game.Seq.core.assets['images/scanner/tutorial2.png'];
		this.tutorial2.x = 159 - this.tutorial2.width / 2;
		this.tutorial2.y = 278;
		this.tutorial2.touchEnabled = false;
		this.addChild(this.tutorial2);

		this.start2.opacity = 0;
		this.tutorial2.opacity = 0;

		// Touch Event
		this.start1.addEventListener(enchant.Event.TOUCH_START, function(e) {
			if (this.scene.selected == "start") {
				this.scene.start2.tl.fadeIn(2, enchant.Easing.EXPO_EASEOUT)
									.fadeOut(2, enchant.Easing.EXPO_EASEOUT)
									.fadeIn(2, enchant.Easing.EXPO_EASEOUT)
									.fadeOut(2, enchant.Easing.EXPO_EASEOUT)
									.delay(10)
									.then(function() { Game.Seq.core.popScene() })
									.then(function() { Game.manager.execStart() });
			} else {
				this.scene.selected = "start";
				this.scene.makeCircle(153, 243, 60, 30);
			}
		});
		this.tutorial1.addEventListener(enchant.Event.TOUCH_START, function(e) {
			if (this.scene.selected == "tutorial") {
				this.scene.tutorial2.tl.fadeIn(2, enchant.Easing.EXPO_EASEOUT)
									.fadeOut(2, enchant.Easing.EXPO_EASEOUT)
									.fadeIn(2, enchant.Easing.EXPO_EASEOUT)
									.fadeOut(2, enchant.Easing.EXPO_EASEOUT)
									.then(function() { Game.Seq.core.popScene() })
									.then(function() { Game.manager.execTutorial() });
			} else {
				this.scene.selected = "tutorial";
				this.scene.makeCircle(153, 286, 50, 22);
			}
		});

		this.cubes = new Array();	
		this.addCube(4,37,51)
		this.addCube(2,37,78)
		this.addCube(6,30,99)
		this.addCube(5,111,60)
		this.addCube(3,122,77)
		this.addCube(1,113,97)
		this.addCube(3,193,48)
		this.addCube(1,202,94)
		this.addCube(2,275,48)
		this.addCube(5,269,68)
		this.addCube(6,266,91)
		this.addCube(4,277,101)
		this.addCube(7,83,63)
		this.addCube(8,89,77)
		this.addCube(12,73,81)
		this.addCube(9,75,96)
		this.addCube(10,142,29)
		this.addCube(11,159,48)
		this.addCube(10,147,74)
		this.addCube(8,160,89)
		this.addCube(12,214,58)
		this.addCube(11,233,63)
		this.addCube(9,227,82)
		this.addCube(7,227,97)

		for (var i=0; i<this.cubes.length; i++) {
			var cube = this.cubes[i];
			cube.tl.delay(i*2);
			cube.tl.moveBy(0, -5, 30, enchant.Easing.ELASTIC_EASEOUT)
				   .moveBy(0, 5, 20)
				   .loop();
		}
	},
	addCube: function(index, x, y) {
		var cube = new Cube(index, x-3, y-3);
		this.addChild(cube);
		this.cubes.push(cube);
	},
	makeCircle: function(x, y, width, height) {
		for (var i=0; i<this.cubes.length; i++) {
			var cube = this.cubes[i];
			var degree = 360 / 24 * i;
			var radian = degree * (Math.PI / 180);
			var tx = Math.sin(radian) * width + x;
			var ty = Math.cos(radian) * height + y;
			cube.tl.clear();
			cube.tl.moveTo(tx, ty, 20, enchant.Easing.CUBIC_EASEOUT);
		}
	}
});


/**
 * CongScene
 */
var CongScene = enchant.Class.create(enchant.Scene, {
	initialize: function() {
		enchant.Scene.call(this);

		this.congrats = new Sprite();
		this.congrats.width  = Game.Seq.core.assets['images/scanner/congrats.png'].width;
		this.congrats.height = Game.Seq.core.assets['images/scanner/congrats.png'].height;
		this.congrats.image  = Game.Seq.core.assets['images/scanner/congrats.png'];
		this.congrats.x = 0;
		this.congrats.y = 3;
		this.addChild(this.congrats);

		this.congrats.opacity = 0;
		this.congrats.touchEnabled = false;
		this.congrats.tl.fadeIn(20, enchant.Easing.CUBIC_EASEOUT)
						.and().moveBy(0, -3, 20, enchant.Easing.CUBIC_EASEOUT)
						.then(function() { Game.cong_scene.congrats.touchEnabled = true });
		// Touch Event
		this.congrats.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.manager.gameClear();
		});
	},
	addCube: function(index, x, y) {
		var cube = new Cube(index, x-3, y-3);
		this.addChild(cube);
		this.cubes.push(cube);
	},
	makeCircle: function(x, y, width, height) {
		for (var i=0; i<this.cubes.length; i++) {
			var cube = this.cubes[i];
			var degree = 360 / 24 * i;
			var radian = degree * (Math.PI / 180);
			var tx = Math.sin(radian) * width + x;
			var ty = Math.cos(radian) * height + y;
			cube.tl.clear();
			cube.tl.moveTo(tx, ty, 20, enchant.Easing.CUBIC_EASEOUT);
		}
	}
});
