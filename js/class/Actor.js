/**
 * Actor.js
 *  - Evaluator
 *  - Scanner
 *  - Enemy
 *  - Bullet
 *  - BugSpot
 */

/**
 * Evaluator
 */
var Evaluator = enchant.Class.create({
	initialize: function() {
		// Property
		this.level  = 3;
		this.depth  = 10;
		this.base   = 1; 
		this.path   = 1; 
		this.red	= -100;
		this.blue   = 10;
		this.yellow = 10;
		this.bug	= -100;
		this.enemy	= -100;
		
		this.label = {
			1: "Level 1",
			2: "Level 2",
			3: "Level 3",
			4: "Level 4",
			5: "Level 5",
		}
	},
	changeLevel: function(value) {
		this.level = Number(value);
		switch (this.level) {
			case 1:
				this.depth  = 2;
				this.base   = 1; 
				this.path   = 0; 
				this.blue   = -1;
				this.yellow = -1;
				break;
			case 2:
				this.depth  = 4;
				this.base   = 2; 
				this.path   = 1; 
				this.blue   = 1;
				this.yellow = 1;
				break;
			case 3:
				this.depth  = 6;
				this.base   = 5; 
				this.path   = 5; 
				this.blue   = 10;
				this.yellow = 10;
				break;
			case 4:
				this.depth  = 8;
				this.base   = 1; 
				this.path   = 5; 
				this.blue   = 14;
				this.yellow = 13;
				break;
			case 5:
				this.depth  = 12;
				this.base   = 1; 
				this.path   = 5; 
				this.blue   = 100;
				this.yellow = 100;
				break;
			default :
				break;
		}
	},
	getLabel: function() {
		var index = this.level
		return this.label[index];
	}
});


/**
 * Scanner.js
 */
var Scanner = enchant.Class.create(enchant.Sprite, {
	initialize: function(width, height, type) {
		Sprite.call(this, width, height, type);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/player_k.png"];
		this.frame = 1;
		this.dir   = "right";
		this.input = "right";
		this.speed    = 1;
		this.interval = 3;
		this.color  = "red";
		this.queue	= ["blue", "yellow"];
		this.onNode = false;
		this.count = 0;
		this.score = 0;
		this.distance   = 0;
		this.node_count = 0;
		this.orig_x   = false;
		this.orig_y   = false;
		this.orig_dir = false;
		this.orig_color = false;
		this.type  = 0; // 0:player, 1:npc
		this.alive = true; 
		this.yellowCount = 0; 
		this.originX = width  /2;
		this.originY = height /2;
		this.enemy_dead	= false; 
		this.track_dead	= false; 
		this.bug_dead	= false; 
		this.speed_level	= 0; 
		// 初期値の設定
		this.setProperty(type);
		this.setPos(type);
		this.setType(type);
		this.setColor(type);
	},
	setProperty: function(type) {
		// ゲーム中に変動する値の初期化
		this.speed      = 1;
		this.count		= 0;
		this.distance   = 0;
		this.alive		= true; 
		this.visible	= true; 
		this.enemy_dead	= false; 
		this.track_dead	= false; 
		this.bug_dead	= false; 
		if (type=="player")	{
			this.rotation = -90;
			this.dir   = "right";
			this.input = "right";
		} else if (type=="npc")	{
			this.rotation = 90;
			this.dir   = "left";
			this.input = "left";
		}
	},
	setColor: function() {
		this.color = this.getRandomColor();
		var q0 = this.getRandomColor();
		var q1 = this.getRandomColor();
		this.queue = [q0, q1];
	},
	setPos: function(type) {
		if (type=="player")	{
			this.x = 20*1 + 3;
			this.y = 20*1 + 3;
		} else if (type=="npc")	{
			this.x = 20*14 + 3;
			this.y = 20*9  + 3;
		}
	},
	setType: function(type) {
		if (type=="player")	{
			this.image = Game.Seq.core.assets["images/scanner/player_k.png"];
			this.type = 0;
		} else if (type=="npc")	{
			this.image = Game.Seq.core.assets["images/scanner/player_w.png"];
			this.type = 1;
		}
	},
	setEnemyImage: function(level) {
		switch (level) {
			case 0:
				this.image = Game.Seq.core.assets["images/scanner/player_w.png"];
				break;
			case 1:
				this.image = Game.Seq.core.assets["images/scanner/enemy1.png"];
				break;
			case 2:
				this.image = Game.Seq.core.assets["images/scanner/enemy2.png"];
				break;
			case 3:
				this.image = Game.Seq.core.assets["images/scanner/enemy3.png"];
				break;
			case 4:
				this.image = Game.Seq.core.assets["images/scanner/enemy4.png"];
				break;
			case 5:
				this.image = Game.Seq.core.assets["images/scanner/enemy5.png"];
				break;
			case 6:
				this.image = Game.Seq.core.assets["images/scanner/enemy6.png"];
				break;
		}
	},
	onenterframe: function(e) {
		// ポーズ中は移動しない
		if (Game.in_option) return;
		if (Game.in_ready) return;
		if (Game.in_score) return;
		// プレイヤーの現在位置
		this.orig_x = this.x;
		this.orig_y = this.y;
		this.orig_dir = this.dir;
		this.orig_color = this.color;
		// プレイヤーの位置を移動する
		this.move();
		// 線の交点に居るかどうかの判別
		this.checkOnNode();
		// NPCの移動方向を決める処理
		if (this.type == 1) { // npcの場合のみAIを参照する
			this.checkAI();
		}
		// 線の交点で方向転換する
		this.turn();
		// トラックの当たり判定
		this.checkTrack();
		// 障害物の当たり判定
		this.checkObstacle();
		// トラックを描画する
		this.drawTrack();
		// トラックの色変更
		this.changeColor();
		// 生存判定
		this.checkAlive();
		// スコアの加算処理
		if (this.type == 0) { // playerの場合のみスコアを加算する
			this.countScore();
		}
	},
	move: function() {
		if (this.dir == "right") {	
			this.x += this.speed;
		}
		else if (this.dir == "left") {	
			this.x -= this.speed;
		}
		else if (this.dir == "up") {	
			this.y -= this.speed;
		}
		else if (this.dir == "down") {	
			this.y += this.speed;
		}
	},
	checkOnNode: function() {
		// 交点の判定は座標の誤差を四捨五入で修正して行う
		var tx = Math.round(this.x-3);
		var ty = Math.round(this.y-3);
		// 交点に居るかどうかの判定
		if ( (tx) % Game.SQUARE_W == 0 && (ty) % Game.SQUARE_H == 0) {
			this.onNode = true;
			// 座標のズレの修正
			this.x = tx + 3;
			this.y = ty + 3;
		} else {
			this.onNode = false;
		}
	},
	turn: function() {
		if (this.onNode) {
			// プレイヤーの入力方向に方向転換する
			this.dir = this.input;
			if (this.orig_dir != this.dir) {
				this.node_count = 3;
			}
			// 突き当りに差し掛かった場合の処理：強制的に方向転換する
			if ( (this.x-3) == 300 && this.dir == "right" ||  (this.x-3) == 0 && this.dir == "left" ) {
				if (this.y < Game.FIELD_H/2) {
					this.dir = "down";
					this.input = "down";
				} else {
					this.dir = "up";
					this.input = "up";
				}
			}
			else if ( (this.y-3) == 200 && this.dir == "down" || (this.y-3) == 0 && this.dir == "up" ) {
				if (this.x < Game.FIELD_W/2) {
					this.dir = "right";
					this.input = "right";
				} else {
					this.dir = "left";
					this.input = "left";
				}
			}
			// 機体を回転させる処理
			this.turn_dir = null;
			if (this.orig_dir=="right") {
				if (this.dir=="up")		this.turn_dir = "left";
				if (this.dir=="down")	this.turn_dir = "right";
			}
			else if (this.orig_dir=="left") {
				if (this.dir=="up")		this.turn_dir = "right";
				if (this.dir=="down")	this.turn_dir = "left";
			}
			else if (this.orig_dir=="up") {
				if (this.dir=="right")	this.turn_dir = "right";
				if (this.dir=="left")	this.turn_dir = "left";
			}
			else if (this.orig_dir=="down") {
				if (this.dir=="right")	this.turn_dir = "left";
				if (this.dir=="left")	this.turn_dir = "right";
			}
			if (this.turn_dir=="right") {
				this.tl.rotateBy(90,12,enchant.Easing.CIRC_EASEOUT);
			} else if (this.turn_dir=="left") {
				this.tl.rotateBy(-90,12,enchant.Easing.CIRC_EASEOUT);
			}
		}
	},
	checkTrack: function() {
		if (this.onNode) {
			var tx = this.x-3;
			var ty = this.y-3;
			var pixel = Game.surface2.getPixel(tx, ty);
			if (pixel[0] == 255 && pixel[1] == 0 && pixel[2] == 0) {
				this.alive = false;
				this.track_dead	= true; 
			}
			else if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 255) {
				this.scene.score.plus(Game.blue_score);	// blue
				this.node_count = 3;
				// 対戦モードの判別
				if (Game.vs_mode) {
					// 相手の周囲に敵を出現させる
					var target = null;
					var master = null;
					if (this.type == 0) {	// 自機
						target = Game.npc;
						master = Game.player;
					} else {				// 敵機
						target = Game.player;
						master = Game.npc;
					}
				    var bullet = new Bullet();
					bullet.x = this.x;
					bullet.y = this.y;
					bullet.target = target;
					bullet.master = master;
				    bullet.attackTarget();
				    Game.Seq.core.rootScene.addChild(bullet);

					// アイコン
					var icon = new Icon2(1);
					icon.x = this.x + 8;
					icon.y = this.y - 16;
					icon.summonEnemy();
					icon.popUp();
					Game.Seq.core.rootScene.addChild(icon);
					//  console.log("dir_x", dir_x);
					//  console.log("dir_y", dir_y);
					//  console.log("enemy", enemy);
				}
			}
			else if (pixel[0] == 0 && pixel[1] == 255 && pixel[2] == 255) {
				this.node_count = 3;
				// スピードアップ
				this.yellowCount += 1;
				var spd = this.getSpeed();
				// アイコン
				var icon = new Icon2(1);
				icon.x = this.x + 8;
				icon.y = this.y - 16;
				icon.speedUp();
				icon.popUp();
				Game.Seq.core.rootScene.addChild(icon);
			}
		} 
		// 既にトラックが引いてあるかどうかの判定
		if (!this.onNode) {
			// 既にトラックが引いてあればGAMEOVER
			var tx = this.x-3;
			var ty = this.y-3;
			// 交差点を通過した直後は判定しない
			var check = true;
			if (this.node_count > 0) {
				check = false;
				this.node_count -= 1;
			}
			if (check) {
				var pixel = Game.surface2.getPixel(tx, ty);
				if (pixel[0] == 255 && pixel[1] == 0 && pixel[2] == 0) {
					console.log("GAMEOVER トラック赤");
					this.alive = false;
					this.track_dead	= true; 
				}
				else if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 255) {
					console.log("GAMEOVER トラック青");
					this.alive = false;
					this.track_dead	= true; 
				}
				else if (pixel[0] == 0 && pixel[1] == 255 && pixel[2] == 255) {
					console.log("GAMEOVER トラック黄");
					this.alive = false;
					this.track_dead	= true; 
				}
			}
		}
	},
	checkObstacle: function() {
		// 衝突判定用フラグ
		var bump = false;
		// バグスポット
		var bugs = Game.Seq.core.rootScene.bugs.childNodes;
		for (var i=0; i<bugs.length; i++) {
			if (this.within(bugs[i], 10)) {
				bump = true;
				this.bug_dead = true; 
			};
		}
		// 敵
		var enemies = Game.Seq.core.rootScene.enemies.childNodes;
		for (var i=0; i<enemies.length; i++) {
			if (this.within(enemies[i], 5)) {
				bump = true;
				this.enemy_dead	= true; 
			};
		}
		// 対戦相手
		if (Game.vs_mode && this.type == 0) {
			var npc = Game.npc;
			if (this.within(npc, 5)) {
				// 体当たりは敗北
				Game.player.alive = false;
				Game.player.checkAlive();
			};
		}
		if (bump) {
			this.alive = false;
		}
	},
	drawTrack: function() {
		Game.surface.context.beginPath();
		Game.surface.context.moveTo(this.orig_x-3, this.orig_y-3);
		Game.surface.context.lineTo(this.x-3, this.y-3);
		Game.surface.context.globalCompositeOperation = "lighter"; //"source-over";
		// outer line
		switch (this.color) {
			case "red":
				Game.surface.context.strokeStyle	= "#FF0000";
				break;
			case "blue":
				Game.surface.context.strokeStyle	= "#0000FF";
				break;
			case "yellow":
				Game.surface.context.strokeStyle	= "#FFFF00";
				break;
		}
		Game.surface.context.lineWidth	= 3;
		Game.surface.context.stroke();
		// inner line
		switch (this.color) {
			case "red":
				Game.surface.context.strokeStyle	= "#D773A9"; //"#F82F66"; 
				break;
			case "blue":
				Game.surface.context.strokeStyle	= "#008ECF"; // "#81BEF7";
				break;
			case "yellow":
				Game.surface.context.strokeStyle	= "#F0EA30"; //"#F5A9A9";
				break;
		}
		Game.surface.context.lineWidth	= 2;
		Game.surface.context.stroke();

		// 判別用のマップ
		switch (this.color) {
			case "red" :
				Game.surface2.setPixel(this.x-3, this.y-3, 255,0,0,255);
				break;
			case "blue" :
				Game.surface2.setPixel(this.x-3, this.y-3, 0,0,255,255);
				break;
			case "yellow" :
				Game.surface2.setPixel(this.x-3, this.y-3, 0,255,255,255);
				break;
			default:
				break;
			Game.surface2.context.stroke();
		}
	},
	changeColor: function() {
		if (this.onNode) {
			// 青交点に出すアイテムのフラグ
			var item_flag = false;
			// 経過フレームをカウントアップする
			this.count += 1;
			// 色を変更する交点の場合
			if ( (this.count % this.interval) == 0) {
				// Random Color
				this.color = this.queue[0];
				var color  = this.getRandomColor();
				// キューを更新する
				this.queue.shift();
				this.queue.push(color);
				// NextManagerを更新する
				var next = null;
				if (this.type == 0) {
					next  = Game.Seq.core.rootScene.next1;
				} else if (this.type == 1) {
					next  = Game.Seq.core.rootScene.next2;
				}
				next.progress(color);
				// 色変更の通知エフェクト
				var wave = new Gas();
				wave.x = this.x - (wave.width - this.width)/2;
				wave.y = this.y - (wave.height - this.height)/2;
				wave.wave(this.color);
				Game.Seq.core.rootScene.addChild(wave);
				// 色変更した場合は境界線が通行可能にする	
				if (this.color != this.orig_color) {
					Game.surface2.context.beginPath();
					Game.surface2.context.fillStyle   = "rgba(200,0,200,1.0)";
					Game.surface2.context.arc(this.x-3, this.y-3, 3, 0, Math.PI*2, false);
					Game.surface2.context.globalCompositeOperation = "source-atop";
					Game.surface2.context.fill();
				}
				if (this.color=="blue" && this.orig_color=="blue") {
					item_flag = true;
				}
			} 
			// 色を変更しない交点の場合
			else {
				// NextManagerの現在のボールを小さくする
				var next = null;
				if (this.type == 0) {
					next  = Game.Seq.core.rootScene.next1;
				} else if (this.type == 1) {
					next  = Game.Seq.core.rootScene.next2;
				}
				next.makeSmaller(this.count%this.interval, this.interval);
				if (this.color=="blue") {
					item_flag = true;
				}
			}
			// 効果付きのトラックはアイテムを出す
			if (item_flag) {
				var item = new Item();
				item.x = this.x;
				item.y = this.y;
				Game.items.addChild(item);
			}
		}
	},
	countScore: function() {
		this.distance += 1;
		//  if (Game.count_distance) {
		//  	this.scene.distance.plus(1);
		//  }
		if (this.onNode) {
			this.scene.score.plus(Game.score_on_node);
		}
	},
	checkAlive: function() {
		if (this.alive) {
			return true;
		} else {
			// 機体が消える
			this.visible = false;
			// 爆発エフェクト
			var bomb = new Bomb();
			bomb.x = this.x;
			bomb.y = this.y;
			Game.Seq.core.rootScene.addChild(bomb);
			// スコアの取得
			var score = 0;
			if (Game.arcade)	score = Number(Game.manager.total_score + Game.score.value);
			else				score = Number(Game.score.value);
			// ゲーム終了処理
			switch (this.type) {
				case 0:
					Game.manager.fail();
					break;
				case 1:
					// クリア処理
					Game.manager.clear();
					//  // アーケードモードの判別
					//  if (Game.arcade) {
					//  	// 次のステージへ進む
					//  	Game.manager.clear();
					//  } else  {
					//  	// テスト
					//  	var img_win = Game.Seq.core.assets["images/clear.png"];
					//  	var win = new Sprite(img_win.width, img_win.height);
					//  	win.image = img_win;
					//  	win.x = (320 - win.width) / 2;
					//  	win.y = (320 - win.height) / 2;
					//  	Game.Seq.core.rootScene.addChild(win);
					//  	Game.Seq.core.end(score, 'あなたのスコアは' + score + 'です');
					//  }
					break;
				default:
					break;
			}
		}
	},
	getSpeed: function() {
		// 黄色トラック通過数によって速さを変更する
		// switch (this.yellowCount) {
		switch (this.speed_level) {
			case 0:
				this.speed = Game.SQUARE_W / 20;
				break;
			case 1:
				this.speed = Game.SQUARE_W / 19;
				break;
			case 2:
				this.speed = Game.SQUARE_W / 18;
				break;
			case 3:
				this.speed = Game.SQUARE_W / 17;
				break;
			case 4:
				this.speed = Game.SQUARE_W / 16;
				break;
			case 5:
				this.speed = Game.SQUARE_W / 15;
				break;
			case 6:
				this.speed = Game.SQUARE_W / 14;
				break;
			default :
				break;
		}
		return this.speed;
	},
	getRandomColor: function() {
		var color = "";
		var rand = Math.floor(Math.random()*100);
		if (rand <= Game.red_rate) {
			color = "red";
		} else {
			color = "blue";
		}
		// if (rand==0)		color = "red";
		// else if (rand==1)	color = "blue";
		// else if (rand==2)	color = "yellow";
		return color;
	},
	checkAI: function() {
		if (this.onNode) {
			// AIに従って方向転換する
			switch (this.ai_type) {
				case 1:
					var rand = Math.floor(Math.random() * 6);
					if (rand<1) this.input = "right";
					else if (rand<2) this.input = "left";
					else if (rand<3) this.input = "up";
					else if (rand<4) this.input = "down";
					break;
				case 2:
					this.simpleAI();
					break;
				case 3:
					this.routeSearchAI();
					break;
				default :
					break;
			}
			// 逆方向へ移動しようとする行動を防ぐ
			if (this.dir=="right" && this.input=="left") this.input = this.orig_dir;
			if (this.dir=="left" && this.input=="right") this.input = this.orig_dir;
			if (this.dir=="up" && this.input=="down") this.input = this.orig_dir;
			if (this.dir=="down" && this.input=="up") this.input = this.orig_dir;
			// 画面外へ移動しようとする行動を防ぐ
			if (this.y==203 && this.input=="down") this.input = this.orig_dir;
			if (this.y==3 && this.input=="up") this.input = this.orig_dir;
			if (this.x==303 && this.input=="right") this.input = this.orig_dir;
			if (this.x==3 && this.input=="left") this.input = this.orig_dir;
		}
	},
	simpleAI: function() {
		// 0:right, 1:left, 2:up, 3:down	
		var dir_array = {0:"right",1:"left",2:"up",3:"down"};

		var tx = this.x-3;
		var ty = this.y-3;
		var d_score = new Array(4);
		for (var i=0; i<d_score.length; i++) {
			var score = 0;
			// 探査する座標の初期化
			tx = this.x-3;
			ty = this.y-3;
			// 各方向の座標へずらす
			if (i==0) tx += Game.SQUARE_W;
			if (i==1) tx -= Game.SQUARE_W;
			if (i==2) ty -= Game.SQUARE_H;
			if (i==3) ty += Game.SQUARE_H;
			// 移動先に赤トラックがなければ	+10
			var pixel = Game.surface2.getPixel(tx, ty);
			if (pixel[0] == 255 && pixel[2] == 0) {
				//score -= 10;
			} else {
				score += 10;
			}
			// 移動先にバグスポットがなければ +10
			var bug_flag = false;
			var bugs = Game.Seq.core.rootScene.bugs.childNodes;
			for (var j=0; j<bugs.length; j++) {
				var bug = bugs[j];	
				if ( Math.abs(bug.x - tx)<10 && Math.abs(bug.y - ty)<10) {
					bug_flag = true;
				} 
			}
			if (bug_flag) {
				//score -= 10;
			} else {
				score += 10;
			}
			// 移動先に敵がいなければ +5	   
			var enemy_flag = false;
			var enemies = Game.Seq.core.rootScene.enemies.childNodes;
			for (var j=0; j<enemies.length; j++) {
				var enemy = enemies[j];	
				if ( Math.abs(enemy.x - tx)<30 && Math.abs(enemy.y - ty)<30) {
					enemy_flag = true;
				} 
			}
			if (!enemy_flag) {
				score += 5;
			}
			// 画面端に位置するとき進行方向に対してスコアを減点する
		    if (this.x==303 && i==0) score -= 100;
		    if (this.x==3 && i==1)	 score -= 100;
		    if (this.y==3 && i==2)   score -= 100;
		    if (this.y==203 && i==3) score -= 100;
			// 逆方向に対するスコアを減らす
		    if (this.orig_dir=="left" && i==0)	score -= 100;
		    if (this.orig_dir=="right" && i==1)	score -= 100;
		    if (this.orig_dir=="down" && i==2)  score -= 100;
		    if (this.orig_dir=="up" && i==3)	score -= 100;
			// 1ピクセル先の色を確認する
			tx = this.x-3;
			ty = this.y-3;
			// 各方向の座標へずらす
			if (i==0) tx += 1;
			if (i==1) tx -= 1;
			if (i==2) ty -= 1;
			if (i==3) ty += 1;
			var next_pixel = Game.surface2.getPixel(tx, ty);
			if (pixel[0] == 255 || pixel[2] == 255) {
				score -= 100;
			}
			// スコアの更新
			d_score[i] = score;
		}
		// d_scoreの最大値の向きへ方向転換する
		var max_no    = 0;
		var max_score = 0;
		for (var i=0; i<d_score.length; i++) {
			if (d_score[i] > max_score)	{
				max_no = i;
				max_score = d_score[i];
			}
		}
		// 同率一位の向きがあるか調査
		var candidates = new Array();
		for (var i=0; i<d_score.length; i++) {
			if (d_score[i] == max_score) {
				candidates.push(i);
			}
		}
		// 候補が複数ある場合はランダム
		if (candidates.length > 1) { 
			var rand = Math.floor(Math.random() * candidates.length);
			max_no = candidates[rand];
		}
		// max_noに従い方向転換する
		if (max_no == 0) this.input = "right";
		if (max_no == 1) this.input = "left";
		if (max_no == 2) this.input = "up";
		if (max_no == 3) this.input = "down";
		// 候補に現在の進行方向があるか判別
		var dir_continue = false;
		for (var i=0; i<candidates.length; i++) {
			var no = candidates[i];
			if (this.orig_dir == dir_array[no]) {
				dir_continue = true;
			}
		}
		// 一定の確率で進行方向を優先する
		if (dir_continue) {
			var rand = Math.floor(Math.random() * 5);
			if (rand<3) this.input = this.orig_dir;
		}
		
		//  console.log("right ", d_score[0]);
		//  console.log("left  ", d_score[1]);
		//  console.log("up    ", d_score[2]);
		//  console.log("down  ", d_score[3]);
		//  console.log("candidates", candidates);
		//  console.log("input ", this.input);
	},
	routeSearchAI: function() {
		// 交点の色を判別する情報
		this.nodeColor = {	
			0:	"none",
			1:	"red",
			2:	"blue",
			3:	"yellow",
		};
		// 各方向へ移動可能かどうかを保持する情報
		// right:1, left:2, up:4, down:8
		this.nodePath = {	
			0: [0,0,0,0],	// 全方向通行可能
			1: [1,0,0,0],	// 右　　　が通行不可
			2: [0,1,0,0],	// 左　　　が通行不可
			3: [1,1,0,0],	// 右左　　が通行不可
			4: [0,0,1,0],	// 上　　　が通行不可 
			5: [1,0,1,0], 	// 右上　　が通行不可 
			6: [0,1,1,0], 	// 左上　　が通行不可 
			7: [1,1,1,0], 	// 右左上　が通行不可 
			8: [0,0,0,1], 	// 下　　　が通行不可 
			9: [1,0,0,1], 	// 右下　　が通行不可 
			10: [0,1,0,1], 	// 左下　　が通行不可 
			11: [1,1,0,1], 	// 右左下　が通行不可 
			12: [0,0,1,1], 	// 上下　　が通行不可 
			13: [1,0,1,1], 	// 右上下　が通行不可 
			14: [0,1,1,1], 	// 左上下　が通行不可 
			15: [1,1,1,1] 	// 右左上下が通行不可 
		};
		// 交点の色情報の初期値
		this.color_arr = [
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		];
		// 交点の通行情報の初期値
		this.path_arr = [
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		];
		// 各交点の状態を取得する
		for (var i=0; i<Game.FIELD_LINE; i++) {
			for (var j=0; j<Game.FIELD_ROW; j++) {
				// 探査する座標の取得
				tx = j*Game.SQUARE_W;
				ty = i*Game.SQUARE_H;
				// 交点の色を探査
				var color = "";
				var pixel = Game.surface2.getPixel(tx, ty);
				if (pixel[0]==255 && pixel[1]==0 && pixel[2]==0)		color = "red";	
				else if (pixel[0]==0 && pixel[1]==0 && pixel[2]==255)	color = "blue";	
				else if (pixel[0]==0 && pixel[1]==255 && pixel[2]==255)	color = "yellow";	
				if (color=="red")			this.color_arr[i][j] = 1;
				else if (color=="blue")		this.color_arr[i][j] = 2;
				else if (color=="yellow")	this.color_arr[i][j] = 3;
				// 交点の通行情報の探査
				var value = 0;
				var p_r = Game.surface2.getPixel(tx+1, ty);
				var p_l = Game.surface2.getPixel(tx-1, ty);
				var p_u = Game.surface2.getPixel(tx, ty-1);
				var p_d = Game.surface2.getPixel(tx, ty+1);
				if (p_r[0]!=0 || p_r[1]!=0 || p_r[2]!=0 ) 	value += 1;
				if (p_l[0]!=0 || p_l[1]!=0 || p_l[2]!=0 )	value += 2;
				if (p_u[0]!=0 || p_u[1]!=0 || p_u[2]!=0 ) 	value += 4;
				if (p_d[0]!=0 || p_d[1]!=0 || p_d[2]!=0 ) 	value += 8;
				this.path_arr[i][j] = value;
			}
		}

		// 残り移動可能マス数を再帰検索
		this.score_arr = [
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		];

		var line = Math.floor((this.y-3) / Game.SQUARE_H);
		var row  = Math.floor((this.x-3) / Game.SQUARE_W);
		var depth  = 0;
		var origin = null;
		this.searchRoute(line, row, depth, origin);

		// 上下左右のスコアから一番高い方向を選ぶ
		var neighbor = [0,0,0,0];
		if (row<15)   neighbor[0] = this.score_arr[line][row+1];
		if (row>0)    neighbor[1] = this.score_arr[line][row-1];
		if (line>0)   neighbor[2] = this.score_arr[line-1][row];
		if (line<10)  neighbor[3] = this.score_arr[line+1][row];
		// 逆方向に対するスコアは無視
		if (this.orig_dir=="right")	neighbor[1] = 0;
		if (this.orig_dir=="left")	neighbor[0] = 0;
		if (this.orig_dir=="up")	neighbor[3] = 0;
		if (this.orig_dir=="down")  neighbor[2] = 0;
		// 方向を選ぶ
		var max   = 0;
		var index = 0;
		for (var i=0; i<neighbor.length; i++) {
			if (neighbor[i] > max) {
				max = neighbor[i];
				index = i;
			}
		}
		// indexに従い方向転換する
		if (index == 0) this.input = "right";
		if (index == 1) this.input = "left";
		if (index == 2) this.input = "up";
		if (index == 3) this.input = "down";

		// console Influence Map
		//  console.log("color_arr:");
		//  this.consoleArray(this.color_arr);
		//  console.log("path_arr");
		//  this.consoleArray(this.path_arr);
		//  console.log("score_arr");
		//  this.consoleArray(this.score_arr);
		//  if (this.input != this.orig_dir) {
		//  	console.log("方向変更", this.input);
		//  }
		
	}, 
	searchRoute: function(line, row, depth, origin) {
		if (row<0 || row>=16 || line<0 || line>=11) {
			return 0;
		}
		if (depth > Game.evaluator.depth) { //11) {
			return 0;
		}
		if (this.score_arr[line][row]) {
			return 0;
		}

		// console.log("line"+ line+", row"+row+",depth"+depth+",origin");

		var score = 1;//0.1; //1;
		var color_key = this.color_arr[line][row];
		var path_key  = this.path_arr[line][row];
		var c_value = this.nodeColor[color_key];
		var p_value = this.nodePath[path_key];
		
		// トラックの色を評価する
		if (c_value=="red")			score += Game.evaluator.red;    //100;
		else if (c_value=="blue")	score += Game.evaluator.blue;   //100;
		else if (c_value=="yellow") score += Game.evaluator.yellow; //100;

		// 移動可能マスから評価する
		if (origin=="right" && p_value[1])	score-=100;
		if (origin=="left" && p_value[0])	score-=100;
		if (origin=="up" && p_value[3])		score-=100;
		if (origin=="down" && p_value[2])   score-=100;

		var empty = 0;
		for (var i=0; i<p_value.length; i++) {
			if (p_value[i] ==0 ) {
				empty += 1;	
			}
		}
		score += empty * Game.evaluator.path; //1;

		// 障害物を評価する
		var tx = row  * Game.SQUARE_W;
		var ty = line * Game.SQUARE_H;
		var bugs = Game.Seq.core.rootScene.bugs.childNodes;
		for (var j=0; j<bugs.length; j++) {
			var bug = bugs[j];	
			if ( Math.abs(bug.x - tx)<10 && Math.abs(bug.y - ty)<10) {
				score += Game.evaluator.bug; //100;	
			} 
		}
		var enemies = Game.Seq.core.rootScene.enemies.childNodes;
		for (var j=0; j<enemies.length; j++) {
			var enemy = enemies[j];	
			if ( Math.abs(enemy.x - tx)<20 && Math.abs(enemy.y - ty)<20) {
				score += Game.evaluator.enemy; //100;
			} 
		}

		// プレイヤーの位置を評価する
		var player = Game.player;
		if ( Math.abs(player.x - tx)<10 && Math.abs(player.y - ty)<10) {
			score -= 50;
		}

		if (score < 0) {
			return 0;
		} else {
			// 上下左右マスのスコアを計算する
			var neighbor_arr = [0,0,0,0];
			if (p_value[0]==0 && origin!="left")	neighbor_arr[0] = this.searchRoute(line, row+1, depth+1, "right");
			if (p_value[1]==0 && origin!="right")	neighbor_arr[1] = this.searchRoute(line, row-1, depth+1, "left");
			if (p_value[2]==0 && origin!="down")	neighbor_arr[2] = this.searchRoute(line-1, row, depth+1, "up");
			if (p_value[3]==0 && origin!="up")		neighbor_arr[3] = this.searchRoute(line+1, row, depth+1, "down");
		
			// 上下左右すべて足し合わせる評価方式
			//  for (var i=0; i<neighbor_arr.length; i++) {
			//  	score += neighbor_arr[i];
			//  }
			// 上下左右から最高値を取り出して足す評価方式
			score += Math.max.apply(null, neighbor_arr);

			// スコアを四捨五入する
			score = Math.round(score);
			if (score > this.score_arr[line][row]) {
				this.score_arr[line][row] = score;
			} 
			return score;
		}
	},
	consoleArray: function(arr) {
		// 配列の中身をstrに足していく
		var str ="";
		for (var i=0; i<arr.length; i++) {
			for (var j=0; j<arr[i].length; j++) {
				var text = arr[i][j];
				while (text.length < 3) {
					text = " " + text;
				}
				str += text + ",";
			}
			str += "\n";
		}
		console.log(str);
	}
})


/**
 * Enemy.js
 */
var Enemy = enchant.Class.create(enchant.Sprite, {
	initialize: function(width, height) {
		Sprite.call(this, width, height);
		// Image
		this.image = Game.Seq.core.assets["images/scanner/funnel.png"];
		this.frame = 1;
		// Property
		this.dir    = "left";
		this.speed  = 0.5;
		this.opacity = 0;
		this.tl.fadeIn(20);
	}
	, dead: function () {
		// エネミー画像を消去
		Game.Seq.core.rootScene.enemies.removeChild(this);
		// 爆発エフェクト
		var bomb = new Bomb();
		bomb.x = this.x;
		bomb.y = this.y;
		Game.Seq.core.rootScene.addChild(bomb);
	}
	, setRandomDir: function () {
		var rand = Math.floor(Math.random() * 4);
		var dir = null;
		if (rand==0) dir = "right";
		else if (rand==1) dir = "left";
		else if (rand==2) dir = "up";
		else if (rand==3) dir = "down";
		this.dir = dir;
		// 画面端から出ようとする場合は再計算する
		var tx = this.x-3;
		var ty = this.y-3;
		var recaluculate = false;
		console.log("enemy",tx,ty,dir);
		if ( tx==300 && this.dir == "right")     recaluculate = true;
		else if ( tx==0 && this.dir == "left")   recaluculate = true;
		else if ( ty==0 && this.dir == "up")     recaluculate = true;
		else if ( ty==200 && this.dir == "down") recaluculate = true;
		if (recaluculate) {
			console.log("recaluculate");
			this.setRandomDir();
		}
		
	}
	, setHomingDir: function () {
		if (this.target){
			var diff_x = this.x - this.target.x;	
			var diff_y = this.y - this.target.y;	
			var abs_x = Math.abs(diff_x);
			var abs_y = Math.abs(diff_y);
			if (abs_x > abs_y) {
				if (diff_x<0) this.dir = "right";
				else		  this.dir = "left";
			} else {
				if (diff_y>0) this.dir = "up";
				else		  this.dir = "down";
			}
			//  console.log("homing", this.dir);
			//  console.log("abs_x", abs_x);
			//  console.log("abs_y", abs_y);
		}
	}
	, setTarget: function (target) {
		if (target) {
			this.target = target;
			this.setHomingDir();
		}
	}
	, setRandomPos: function () {
		var rx = Math.floor(Math.random() * 15);
		var ry = Math.floor(Math.random() * 10);
		var px = parseInt(Game.Seq.core.currentScene.player.x/20);
		var py = parseInt(Game.Seq.core.currentScene.player.y/20);
		if (rx==px&&ry==py) rx=ry=10;
		if (rx==px+1&&ry==py) rx=ry=10;
		if (rx==px-1&&ry==py) rx=ry=10;
		if (rx==px&&ry==py+1) rx=ry=10;
		if (rx==px&&ry==py-1) rx=ry=10;
		if (rx==0) rx=1;
		// 自機と敵機の初期位置には出さない
		if (rx<4&&ry<3) rx=ry=6;
		if (rx>11&&ry>7) rx=ry=6;
		this.x = rx * 20 + 3;
		this.y = ry * 20 + 3;
	}
	, onenterframe: function() {
		// ポーズ中は移動しない
		if (Game.in_option) return;
		if (Game.in_ready) return;
		if (Game.in_score) return;
		// エネミーの位置を移動する
		if (this.dir == "right") {	
			this.x += this.speed;
		}
		else if (this.dir == "left") {	
			this.x -= this.speed;
		}
		else if (this.dir == "up") {	
			this.y -= this.speed;
		}
		else if (this.dir == "down") {	
			this.y += this.speed;
		}
		// 線の交点に居るかどうかの判別
		if ( (this.x-3) % Game.SQUARE_W == 0 && (this.y-3) % Game.SQUARE_H == 0) {
			this.onNode = true;
		} else {
			this.onNode = false;
		}
		// 方向転換
		if (this.onNode) {
			// ターゲットが指定されている場合は追尾する
			if (this.target)	this.setHomingDir();
			else				this.setRandomDir();
			
			// 突き当りに差し掛かった場合の処理：強制的に方向転換する
			if ( (this.x-3) == 300 && this.dir == "right" ||  (this.x-3) == 0 && this.dir == "left" ) {
				if (this.y < Game.FIELD_H/2) {
					this.dir = "down";
				} else {
					this.dir = "up";
				}
			}
			else if ( (this.y-3) == 200 && this.dir == "down" || (this.y-3) == 0 && this.dir == "up" ) {
				if (this.x < Game.FIELD_W/2) {
					this.dir = "right";
				} else {
					this.dir = "left";
				}
			}
		}

		// トラックの衝突判定
		var bump = false;
		var pixel = Game.surface2.getPixel(this.x-3, this.y-3);
		if (pixel[0] == 255 && pixel[1] == 0 && pixel[2] == 0) {
			bump = true;
		} else if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 255) {
			bump = true;
		} else if (pixel[0] == 0 && pixel[1] == 255 && pixel[2] == 255) {
			bump = true;
		}
		// 自爆
		if (bump) {
			this.parentNode.removeChild(this);
			//this.scene.score.plus(1000); // TODO 何故thisはこの書き方だとErrorになる？ this.sceneがnull何故？
			Game.Seq.core.rootScene.score.plus(Game.enemy_bump_track);
			this.dead();
		}
		// 自爆2
		var bugs = Game.Seq.core.rootScene.bugs;
		for (var i=0; i<bugs.childNodes.length; i++) {
			var bug = bugs.childNodes[i];
			var bump = this.within(bug, 10);
			if (bump) {
				this.dead();
				Game.Seq.core.rootScene.score.plus(Game.enemy_bump_bug);
			}
		}
	}
});


/**
 * Bullet.js
 */
var Bullet = enchant.Class.create(enchant.Sprite, {
	initialize: function(type) {
		Sprite.call(this, 12, 12);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/gas2.png"];
		this.frame = 1;
		this.master  = null;
		this.target  = null;
		this.targetX = 0;
		this.targetY = 0;
		this.time = 20;
		this.type = 1;	// 0:周囲3マスにランダム、1:障害物を避ける、2:目の前に出現する
	},
	selectPos: function() {
		if (this.type==0) {
			var pos_arr = this.getRandomPos();
			var targetX = pos_arr[0];
			var targetY = pos_arr[1];
		} else if (this.type==1) {
			var cnt   = 0;
			var empty = false;
			while (!empty) {
				// ランダムで場所を選択する
				var pos_arr = this.getRandomPos();
				var targetX = pos_arr[0];
				var targetY = pos_arr[1];
				// 選択した座標に障害物がないか判別
				empty = this.checkEmpty(targetX, targetY);
				// 一定回数ループすると抜ける
				if (cnt>100)		break;
				cnt++;
				console.log("cnt", cnt);
				console.log("empty", empty);
				console.log("pos_arr", pos_arr);
			}

		} else if (this.type==2) {
			// エネミーの親の進行先に出現
			var master = this.master;
			var distance = 2;
			var targetX = master.x;
			var targetY = master.y;
			if (master.dir=="right")	targetX += 20*distance;
			if (master.dir=="left")		targetX -= 20*distance;
			if (master.dir=="up")		targetY -= 20*distance;
			if (master.dir=="down")		targetY += 20*distance;
			// 弾の速度を早くする
			this.time = 5;
		}

		// エネミー出現位置
		this.targetX = targetX;
		this.targetY = targetY;
	},
	checkEmpty: function(tx, ty) {
		var empty = true;
		// バグスポットが無いかチェック
		for (var i=0; i<Game.bugs.childNodes.length; i++) {
			var bug = Game.bugs.childNodes[i];
			if (bug.x==tx || bug.y==ty) {
				empty = false;
				break;
			}
		}
		// トラックが引かれているかチェック
		var pixel = Game.surface2.getPixel(tx-3, ty-3);
		if (pixel[0] == 255 && pixel[1] == 0 && pixel[2] == 0) {
			empty = false;
		}
		else if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 255) {
			empty = false;
		}
		return empty;
	},
	getRandomPos: function() {
		// ターゲットとの距離をX,Yで算出する
		var target = this.target;
		var tx = Math.round((target.x-3)/20) * 20 + 3;
		var ty = Math.round((target.y-3)/20) * 20 + 3;
		var dir_x = Math.floor(Math.random()*4);
		var dir_y = (3 - dir_x);
		var plus_minus_x = Math.floor(Math.random()*2);
		var plus_minus_y = Math.floor(Math.random()*2);
		if (plus_minus_x == 0) dir_x *= -1;
		if (plus_minus_y == 0) dir_y *= -1;
		// 着弾先が画面外の場合の処理
		var targetX = tx + dir_x*20;
		var targetY = ty + dir_y*20;
		if (targetX<3 || targetX>303)	targetX = tx - dir_x*20;
		if (targetY<3 || targetY>203)	targetY = ty - dir_y*20;
		return [targetX, targetY];
	},
	attackTarget: function() {
		// 着弾座標を算出する
		this.selectPos();
		console.log("test1");
		// 弾丸でエフェクトが着弾したら敵出現
		this.tl.moveTo(this.targetX, this.targetY, this.time)
			   .and().fadeOut(this.time)
			   .then(this.afterAttack)
			   .removeFromScene();
	},
	afterAttack: function() {
		var enemy = new Enemy(16, 16);
		enemy.x = this.targetX;
		enemy.y = this.targetY;
		Game.Seq.core.rootScene.enemies.addChild(enemy);
		if (this.target) {
			enemy.setTarget(this.target);
		}
		// チュートリアルの場合
		if (Game.in_tutorial) {
			if (!Game.tutorial.enemy_tutorial) {
				Game.tutorial.setText("エネミーが出現します。<br>エネミーに当たると、GAMEOVERです");
				Game.tutorial.focus(enemy.x-6, enemy.y-6);
				Game.tutorial.enemy_tutorial = true;
			}
		}
	}
});


/**
 * BugSpot.js
 */
var BugSpot = enchant.Class.create(enchant.Sprite, {
	initialize: function(width, height) {
		Sprite.call(this, width, height);
		// Imape
		this.image = Game.Seq.core.assets["images/scanner/bugspot.png"];
		this.frame = 1;
		// Property
		this.scaleX = 0.1;
		this.scaleY = 0.1;
		this.x = 62;
		this.y = 3;
		this.tl.scaleTo(1, 1, 10);
	}
	, update: function () {
	}
	, setRandomPos: function () {
		var rx = Math.floor(Math.random() * 15);
		var ry = Math.floor(Math.random() * 10);
		var px = parseInt(Game.Seq.core.currentScene.player.x/20);
		var py = parseInt(Game.Seq.core.currentScene.player.y/20);
		if (rx==px&&ry==py) rx=ry=10;
		if (rx==px+1&&ry==py) rx=ry=10;
		if (rx==px-1&&ry==py) rx=ry=10;
		if (rx==px&&ry==py+1) rx=ry=10;
		if (rx==px&&ry==py-1) rx=ry=10;
		// 自機と敵機の初期位置には出さない
		if (rx<4&&ry<3) rx=ry=6;
		if (rx>11&&ry>7) rx=ry=6;
		this.x = rx * 20 + 2;
		this.y = ry * 20 + 2;
	}
});
