/**
 * seauence.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Sequence = new function() {
	var core;		//@ Sequence has a enchant.Core Class().

	var self = function Sequence() {
		this.core = new Core(320, 320);
	}

	self.prototype = {
		constructor: self
		/**
		 * Initialize MVC Object in Game
		 */
		, gameInit: function() {
		}

		/**
		 * Scene Manager
		 */
		, removeScene: function(scene) {
			this.core.rootScene.removeChild(scene);
		}
		, changeScene: function(scene_instance) {
			this.core.replaceScene(scene_instance);
		}

	};

	return self;
}

