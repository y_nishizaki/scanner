/**
 * UI.js
 *  - BackGround
 *  - MenuSkin
 *  - Flicker
 *  - Pause
 *  - CountDown
 *  - NextManager
 *  - Timer
 *  - LabelCounter
 */


/**
 * BackGround
 */
var BackGround = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		Sprite.call(this, Game.FIELD_W, Game.FIELD_H);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/back_ios.png"];
		this.frame = 1;
		this.stage = 1;
	},
	changeImage: function(stage) {
		this.stage = stage;	
		switch (this.stage) {
			case 1:
				this.image = Game.Seq.core.assets["images/scanner/back_ios.png"];
				break;
			case 2:
				this.image = Game.Seq.core.assets["images/scanner/back_ios5.png"];
				break;
			case 3:
				this.image = Game.Seq.core.assets["images/scanner/back_ios9.png"];
				break;
			case 4:
				this.image = Game.Seq.core.assets["images/scanner/back_ios4.png"];
				break;
			case 5:
				this.image = Game.Seq.core.assets["images/scanner/back_ios2.png"];
				break;
			case 6:
				this.image = Game.Seq.core.assets["images/scanner/back_ios3.png"];
				break;
		}
	}
});


/**
 * MenuSkin
 */
var MenuSkin = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		Sprite.call(this, Game.MENU_W, Game.MENU_H);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/menu1.png"];
		this.frame = 1;
		this.stage = 1;
	},
	changeImage: function(stage) {
		this.stage = stage;	
		switch (this.stage) {
			case 1:
				this.image = Game.Seq.core.assets["images/scanner/menu1.png"];
				break;
			case 2:
				this.image = Game.Seq.core.assets["images/scanner/menu5.png"];
				console.log("stage 2");
				break;
			case 3:
				this.image = Game.Seq.core.assets["images/scanner/menu9.png"];
				break;
			case 4:
				this.image = Game.Seq.core.assets["images/scanner/menu4.png"];
				break;
			case 5:
				this.image = Game.Seq.core.assets["images/scanner/menu2.png"];
				break;
			case 6:
				this.image = Game.Seq.core.assets["images/scanner/menu3.png"];
				break;
		}
	}
});


/**
 * Flicker
 */
var Flicker = enchant.Class.create(enchant.Entity, {
	initialize: function() {
		Entity.call(this);
		// Property
		this.touched = false;
		this.touchX = 0;
		this.touchY = 0;
		this.moveX = 0;
		this.moveY = 0;
		this.dir = "";
		this.radius = 1;
		this.time = 10;
		this.last_touched = 0;
		this.width  = 320;
		this.height = 320;
	},
	ontouchstart: function(e){
		this.touchX = e.x - this.x;
		this.touchY = e.y - this.y;
		this.touched = true;
		this.last_touched = this.age;
		console.log("touch");
	},
	ontouchmove: function(e){
		if (this.touched) {
			// プレイヤーへの参照
			var player = Game.player;
			// フリック動作中の座標の検出
			this.moveX = e.x - this.x;
			this.moveY = e.y - this.y;
			// フリックの位置が一定距離移動したかどうかの判別
			var diff_x = this.moveX - this.touchX;
			var diff_y = this.moveY - this.touchY;
			var abs_x = Math.abs(diff_x);
			var abs_y = Math.abs(diff_y);
			var tri =  (abs_x*abs_x + abs_y*abs_y) - (this.radius * this.radius);
			if (tri >= 0) {
				var flag = true;
			} else {
				var flag = false;
			}
			// フリックの位置が一定距離移動した時の処理
			if (flag) {
				if (abs_x > abs_y) {
					if (diff_x > 0)	this.dir = "right";
					else 			this.dir = "left";
				} else {
					if (diff_y > 0)	this.dir = "down";
					else 			this.dir = "up";
				}
				switch(this.dir) {
					case "right" :
						player.input = "right";
						break;
					case "left" :
						player.input = "left";
						break;
					case "up" :
						player.input = "up";
						break;
					case "down" :
						player.input = "down";
						break;
				};
				this.touched = false;

				// 逆方向へ移動しようとする行動を防ぐ
				if (player.dir=="right" && player.input=="left") player.input = player.orig_dir;
				if (player.dir=="left" && player.input=="right") player.input = player.orig_dir;
				if (player.dir=="up" && player.input=="down") player.input = player.orig_dir;
				if (player.dir=="down" && player.input=="up") player.input = player.orig_dir;
				// 入力報告
				var input_icon = new Icon0();
				input_icon.x = player.x;
				input_icon.y = player.y;
				input_icon.setDir(player.input);
				input_icon.popUp();
				Game.Seq.core.rootScene.addChild(input_icon);

				// チュートリアル中にフリックがあれば、説明を閉じる
				if (Game.in_tutorial) {
					Game.tutorial.close();
				}
			}
		} else {
			// 連続フリックの待機時間の判別
			if (this.last_touched + 20 < this.age) {
				// 指を離さないで連続フリックする
				this.ontouchstart(e);
			}

		}
	},
	ontouchend: function(e){
		if (this.touched) {
			this.touched = false;
		}
	},
});


/**
 * Pause
 */
var Pause = enchant.Class.create(enchant.Sprite, {
	initialize: function() {
		Sprite.call(this, 26, 26);
		// Property
		this.image = Game.Seq.core.assets["images/scanner/m_stop.png"];
		this.frame = 1;
		this.state = "stop";
	},
	changeImage: function(state) {
		this.state = state;
		switch (this.state) {
			case "play":
				this.image = Game.Seq.core.assets["images/scanner/m_play.png"];
				break;
			case "stop":
				this.image = Game.Seq.core.assets["images/scanner/m_stop.png"];
				break;
		}
	},
	ontouchstart: function(e) {
		if (Game.Seq.core.ready) {
			this.pause();
		} else {
			this.resume();
		}
	},
	pause: function() {
		Game.Seq.core.pause();
		this.changeImage("play");
	},
	resume: function() {
		Game.Seq.core.resume();
		this.changeImage("stop");
	}
});


/**
 * CountDown
 */
var CountDown = enchant.Class.create(enchant.Group, {
	initialize: function() {
		Group.call(this, 320, 320);
		// Property
		this.num = new Sprite(32, 32);
		this.num.image = Game.Seq.core.assets["images/scanner/321.png"];
		this.num.frame = 0;
		this.addChild(this.num);
		this.num.x = 160 - this.num.width/2;
		this.num.y = 120 - this.num.height/2;
		this.num.scaleX = 1;
		this.num.scaleY = 1;
		this.num.visible = false;
		// GO!
		this.go = new Group();
		this.go.x = 160 - 64/2;
		this.go.y = 120 - 64/2;
		this.go.originX = 32;
		this.go.originY = 32;
		this.go.scaleX = 2;
		this.go.scaleY = 2;
		this.go.touchEnabled = false;
		this.go.hide = function(){
			for (var i=0; i<this.childNodes.length; i++) {
				this.childNodes[i].visible = false;
			}
		};
		this.go.show = function(){
			for (var i=0; i<this.childNodes.length; i++) {
				this.childNodes[i].visible = true;
			}
		};
		this.go.fadeOut = function(){
			for (var i=0; i<this.childNodes.length; i++) {
				this.childNodes[i].tl.scaleTo(1.3,1.3,10, enchant.Easing.CIRC_EASEOUT);
				this.childNodes[i].tl.fadeOut(25, enchant.Easing.CIRC_EASEIN);
			}
		};
		//  this.str1 = new Sprite(16, 16);
		//  this.str2 = new Sprite(16, 16);
		//  this.str3 = new Sprite(16, 16);
		//  this.str1.image = Game.Seq.core.assets["images/font2.png"];
		//  this.str1.frame = 39;
		//  this.str1.x = 0;
		//  this.str1.touchEnabled = false;
		//  this.str2.image = Game.Seq.core.assets["images/font2.png"];
		//  this.str2.frame = 47;
		//  this.str2.x = 16;
		//  this.str2.touchEnabled = false;
		//  this.str3.image = Game.Seq.core.assets["images/font2.png"];
		//  this.str3.frame = 1;
		//  this.str3.x = 32;
		//  this.str3.touchEnabled = false;
		//  this.addChild(this.go);
		//  this.go.addChild(this.str1);
		//  this.go.addChild(this.str2);
		//  this.go.addChild(this.str3);
		//  this.go.hide();
		this.str1 = new Sprite(64, 64);
		this.str1.image = Game.Seq.core.assets["images/scanner/go.png"];
		this.str1.touchEnabled = false;
		this.addChild(this.go);
		this.go.addChild(this.str1);
		this.go.hide();

		// Hand
		this.hand = new Icon2();
		this.hand.x = 23;
		this.hand.y = 23;
		this.hand.showHand();
		this.hand.moveHand();
		this.hand.visible = false;
		this.addChild(this.hand);
		// EnemyIcon
		this.enemyIcon = new Icon2();
		this.enemyIcon.x = Game.npc.x-3;
		this.enemyIcon.y = Game.npc.y-15;
		this.enemyIcon.showEnemy();
		this.enemyIcon.visible = false;
		this.addChild(this.enemyIcon);

		// CountDown
		this.tl.cue({
			0:function(){
				Game.in_ready = true;
				this.num.visible = true;
				this.hand.visible = true;
				Game.setBugSpot();
				Game.setEnemy();
			},
			30:function(){
				this.num.frame = 1;
			},
			60:function(){
				this.num.frame = 2;
				this.enemyIcon.visible = true;
				this.enemyIcon.popUp();
			},
			90:function(){
				Game.in_ready = false;
				this.num.parentNode.removeChild(this.num);
				this.go.show();
				this.go.fadeOut();
				Game.timer.start_frame = Game.Seq.core.frame;
			},
			120:function(){
				this.parentNode.removeChild(this);
			}
		});
	},
	onenterframe: function() {
		if (Game.in_option) this.tl.pause();
		else			    this.tl.resume();
	},
	dispatch: function() {
		Game.in_ready = true;
	}
});


/**
 * NextManager
 */
var NextManager = enchant.Class.create(enchant.Group, {
	initialize: function(type) {
		Group.call(this, 120, 60);
		// Property
		if (type=="player") {
			this.pos = {
				0: [65,20], 1: [35,20], 2: [5,20], 3: [-5,20], "next": [90,22.5], "waku" : [63,18],
			};
		}
		else if (type=="npc") {
			this.pos = {
				0: [45,20], 1: [75,20], 2: [105,20], 3: [115,20], "next": [8,22.5], "waku" : [43,18],
			};
		}
		this.colors = {
			"red"   : 0,
			"blue"  : 1,
			"yellow": 2,
		};

		this.target = null;

		this.waku = new Sprite(29, 14);
		this.waku.surface = new Surface(29,14);
		this.waku.surface.context.lineWidth	= 2;
		this.waku.surface.context.lineJoin	= "round";
		this.waku.surface.context.strokeStyle = "white";
		this.waku.surface.context.strokeRect(0, 0, 29, 14);
		this.waku.image = this.waku.surface;
		this.waku.x = this.pos["waku"][0];
		this.waku.y = this.pos["waku"][1];
		this.addChild(this.waku);

		this.balls = new Array();
		for (var i=0; i<3; i++) {
			var ball = new Sprite(25, 10);
			ball.image = Game.Seq.core.assets["images/scanner/nextbar.png"];
			var color = "";
			if (i==0) color = "red";
			else if (i==1) color = "blue";
			else if (i==2) color = "yellow";
			ball.frame = this.colors[color];
			ball.x = this.pos[i][0];
			ball.y = this.pos[i][1];
			this.addChild(ball);
			this.balls.push(ball);
		}
		//  console.log(this.pos);
		//  console.log(this.pos[0][0]);

	}, 
	progress: function(color) {
		// 先頭のballを消去
		var current_ball = this.balls[0];
		current_ball.tl.moveBy(0,20,10,enchant.Easing.CUBIC_EASEOUT)
					   .and().fadeOut(10)
					   .removeFromScene();
		this.balls.shift();
		// 残りのballを移動
		for (var i=0; i<this.balls.length; i++) {
			var reserve_ball = this.balls[i];
			reserve_ball.tl.moveTo(this.pos[i][0],this.pos[i][1],10,enchant.Easing.CUBIC_EASEOUT)
		}
		// 新しいballを追加
		var ball = new Sprite(25, 10);
		ball.image = Game.Seq.core.assets["images/scanner/nextbar.png"];
		ball.frame = this.colors[color];
		ball.x = this.pos[3][0];
		ball.y = this.pos[3][1];
		ball.opacity = 0;
		ball.tl.moveTo(this.pos[2][0],this.pos[2][1],10,enchant.Easing.CUBIC_EASEOUT)
		 	   .and().fadeIn(10);
		this.addChild(ball);
		this.balls.push(ball);
	},
	makeSmaller: function(frame, maxframe) {
		// 現在の色を徐々に小さくしていく
		var latest_ball = this.balls[0];
		var scale = Math.floor( (maxframe-frame) / maxframe * 100) /100;
		latest_ball.tl.scaleTo(scale, scale, 10)
	},
	setQueue: function(type) {
		var target = null;
		if (type=="player")		target = Game.player;
		else if (type=="npc")	target = Game.npc;
		var current_ball = this.balls[0];
		var next_ball1	 = this.balls[1];
		var next_ball2	 = this.balls[2];
		var queue0		 = target.queue[0];
		var queue1		 = target.queue[1];
		current_ball.frame = this.colors[target.color];
		next_ball1.frame   = this.colors[queue0];
		next_ball2.frame   = this.colors[queue1];
	},
	hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			this.childNodes[i].visible = false;
		}
	},
	show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			this.childNodes[i].visible = true;
		}
	}

});


/**
 * Timer
 */
var Timer = enchant.Class.create(enchant.Label, {
	initialize: function(width, height) {
		Label.call(this, width, height);
		// Property
		this.start_frame = Game.Seq.core.frame;
		this.font  = "14px Century";
		this.color = "white";
		this.text  = "00:00";
		this.time  = 0;
	},
	setStartTime: function() {
		this.start_frame = Game.Seq.core.frame;
	},
	initTime: function() {
		this.text  = "00:00";
		this.time  = 0;
	},
	getTime: function() {
		var spent = Game.Seq.core.frame - this.start_frame;
		var time  = Math.floor(spent / Game.Seq.core.fps);
		return time;
	},
	getText: function() {
		var time = this.time;
		var m = Math.floor(this.time / 60).toString();
		var s = Math.floor(this.time % 60).toString();
		while (m.length < 2) {
			m = "0" + m;
		}
		while (s.length < 2) {
			s = "0" + s;
		}
		return m + ":" + s;
	},
	onenterframe: function() {
		if (Game.in_ready)  return false;
		if (Game.in_option) return false;
		if (Game.in_score)  return false;
		this.time = this.getTime();
		this.text = this.getText();
	}
});


/**
 * LabelCounter
 */
var LabelCounter = enchant.Class.create(enchant.Label, {
	initialize: function(width, height) {
		Label.call(this, width, height);
		// Font
		this.text = "0";
		this.font = "12px KozMinPro-Regular";
		this.textAlign = "left";
		// Property
		this.value = 0;
		this.display_value = 0;
		this.number_of_digit = 0;
		this.animation = true;
		this.count_per_frame = 50;
		this.before_text = 0;
		// Counter Event
		this.on('enterframe', function(){
			if (this.animation) {
				if (this.value != this.display_value) {
					if (this.value > this.display_value) {
						this.display_value = parseInt(this.display_value) + this.count_per_frame;
						if (this.display_value > this.value) this.display_value = this.value;
					} else {
						this.display_value = parseInt(this.display_value) - this.count_per_frame;
						if (this.display_value < this.value) this.display_value = this.value;
					}
					this.update();
				}
			}
		});
	}
	, update: function () {
		this.text = "";
		if (this.before_text) {
			this.text += this.before_text;
		}
		var val = String(this.display_value);
		if (this.number_of_digit > 0) {
			while (val.length < this.number_of_digit) {
				val = " " + val;
			}
		}
		this.text += val;
	}
	, set: function (num) {
		this.value = num;
		this.display_value = num;
		this.update();
	}
	, plus: function (num) {
		this.value += num;
	}
	, minus: function (num) {
		this.value -= num;
	}
	, setCountPerFrame: function (value) {
		this.count_per_frame = value;
	}
	, setBeforeText: function (text) {
		this.before_text = text;
	}
	, setNumberOfDigit: function (num) {
		this.number_of_digit = num;
	}
});
