/**
 *  DobyGames Import Script
 */

// Plugin files
var docroot_plugins = 'js/plugins/';
var arr_plugins = new Array(
		"nineleap.enchant.js",
		"widget.enchant.js"
	);
for(i in arr_plugins){
	document.write('<script type="text/javascript" src="'+ docroot_plugins + arr_plugins[i] + '"></script>');
}

// Class files
var docroot_class = 'js/class/';
var arr_class = new Array(
		"myprototype.js",
		"sequence.js",
		"Title.js",
		"UI.js",
		"Actor.js",
		"Effect.js"
	);

// Import
for(i in arr_class){
	document.write('<script type="text/javascript" src="'+ docroot_class + arr_class[i] + '"></script>');
}
